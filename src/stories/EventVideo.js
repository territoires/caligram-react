import React from 'react';
import { mount } from 'enzyme';
import { expect } from 'chai';
import { EventVideo } from '../components';

const { storiesOf, describe, it, specs } = global;

const html = "<object width='425' height='344'><param name='movie' value='http://www.youtube.com/v/TnZhi5gaX8g'></param><param name='allowFullScreen' value='true'></param><param name='allowscriptaccess' value='always'></param><embed src='http://www.youtube.com/v/TnZhi5gaX8g' type='application/x-shockwave-flash' width='425' height='344' allowscriptaccess='always' allowfullscreen='true'></embed></object>";

storiesOf('EventVideo', module)
  .add('default', () => {
    const story = <EventVideo html={html} />;
    const wrapper = mount(story);

    specs(() => describe('EventVideo default', () => {
      it('shows a video', () => {
        expect(wrapper.find(EventVideo).render().find('param[name=movie]').prop('value')).to.equal('http://www.youtube.com/v/TnZhi5gaX8g');
      });
    }));

    return story;
  });
