import React from 'react';
import { shallow } from 'enzyme';
import { expect } from 'chai';
import { EventContact } from '../components';

const { storiesOf, describe, it, specs } = global;

const contactInfo = {
  name: 'Roger Lemuscadet',
  phone: '514-842-3642',
  email: 'roger@lemuscadet.com',
  url: 'http://www.google.com',
};

storiesOf('EventContact', module)
  .add('default', () => {
    const story = <EventContact {...contactInfo} />;
    const wrapper = shallow(story);

    specs(() => describe('EventContact default', () => {
      it('shows the name', () => {
        expect(wrapper.contains(contactInfo.name)).to.equal(true);
      });

      it('shows the phone number as a link', () => {
        expect(wrapper.contains(contactInfo.phone)).to.equal(true);
        expect(wrapper.find('a').at(0).prop('href')).to.equal(`tel:+1-${contactInfo.phone}`);
      });

      it('shows the email address as a link', () => {
        expect(wrapper.contains(contactInfo.email)).to.equal(true);
        expect(wrapper.find('a').at(1).prop('href')).to.equal(`mailto:${contactInfo.email}`);
      });

      it('shows the URL as a link', () => {
        expect(wrapper.contains(contactInfo.url)).to.equal(true);
        expect(wrapper.find('a').at(2).prop('href')).to.equal(contactInfo.url);
      });
    }));

    return story;
  })

  .add('with no contact info', () => {
    const story = <EventContact />;
    const wrapper = shallow(story);

    specs(() => describe('EventContact with no contact info', () => {
      it('shows nothing', () => {
        expect(wrapper.find('.EventContact').exists()).to.equal(false);
      });
    }));

    return story;
  });
