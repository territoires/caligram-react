import React from 'react';
import { mount, shallow } from 'enzyme';
import { expect } from 'chai';
import { SelectableLabel } from '../components';

const { storiesOf, describe, it, specs } = global;

storiesOf('SelectableLabel', module)
  .add('default', () => {
    const story = <SelectableLabel label="Un item" />;
    const wrapper = shallow(story);

    specs(() => describe('SelectableLabel default', () => {
      it('shows the provided label', () => {
        expect(wrapper.find('button').contains('Un item')).to.equal(true);
      });

      it('is not selected', () => {
        expect(wrapper.find('button').hasClass('selected')).to.equal(false);
      });
    }));

    return story;
  })

  .add('selected', () => {
    const story = <SelectableLabel label="Un item sélectionné" selected />;
    const wrapper = shallow(story);

    specs(() => describe('SelectableLabel selected', () => {
      it('has the selected class when selected', () => {
        expect(wrapper.find('button').hasClass('selected')).to.equal(true);
      });
    }));

    return story;
  })

  .add('not selected', () => {
    const story = <SelectableLabel label="Un item sélectionné" selected={false} />;
    const wrapper = shallow(story);

    specs(() => describe('SelectableLabel not selected', () => {
      it('doesn’t have the selected class when not selected', () => {
        expect(wrapper.find('button').hasClass('selected')).to.equal(false);
      });
    }));

    return story;
  })

  .add('with group', () => {
    const story = <SelectableLabel label="Un item" group="Groupe" />;
    const wrapper = shallow(story);

    specs(() => describe('SelectableLabel with group', () => {
      it('displays a group', () => {
        expect(wrapper.find('span.group').contains('Groupe')).to.equal(true);
      });
    }));

    return story;
  })

  .add('selected with group', () => {
    const story = <SelectableLabel label="Un item sélectionné" group="Groupe" selected />;
    return story;
  })

  .add('with different styles', () => {
    const appearances = ['plain', 'filter', 'tag'];
    const story = (
      <div style={{ width: '200px' }}>
        {appearances.map((appearance) => {
          return (
            <div key={appearance}>
              <SelectableLabel label={appearance} appearance={appearance} />
              <SelectableLabel label={`${appearance} (selected)`} appearance={appearance} selected />
            </div>
          );
        })}
      </div>
    );
    const wrapper = mount(story);

    specs(() => describe('SelectableLabel with different styles', () => {
      it('has the correct class names', () => {
        expect(wrapper.find('.plain').length).to.equal(2);
        expect(wrapper.find('.filter').length).to.equal(2);
        expect(wrapper.find('.tag').length).to.equal(2);
      });
    }));

    return story;
  });
