import React from 'react';
import { mount } from 'enzyme';
import { MapMarker } from '../components';
import { MockGeolocatedDynamicMap } from '../components/GeolocatedDynamicMap';
import { events } from './fixtures';

const { storiesOf, describe, specs } = global;

const handleOnClick = e => e;
const onCenterChange = obj => obj;

const props = {
  apiKey: '',
  centerLat: 45.562371,
  centerLng: -73.600579,
  events,
  handleOnClick,
  initialZoom: 16,
  onCenterChange,
};

storiesOf('GeolocatedDynamicMap', module)
  .add('default', () => {
    const story = (
      <MockGeolocatedDynamicMap mapProps={props} coords={{}} />
    );

    mount(story);

    specs(() => describe('GeolocatedDynamicMap default', () => {
      // TODO : No tests : it doesn't work, I'll come back to it later
    }));

    return story;
  })
  .add('with MapMarker', () => {
    const mapProps = {
      marker: MapMarker,
      offset: {
        top: -15,
        left: -1,
      },
      ...props,
    };

    const story = (
      <MockGeolocatedDynamicMap mapProps={mapProps} coords={{}} />
    );

    mount(story);

    specs(() => describe('GeolocatedDynamicMap with MapMarker', () => {}));

    return story;
  });
