import React from 'react';
import { mount } from 'enzyme';
import { expect } from 'chai';
import { Button } from '../components';

const { storiesOf, describe, it, specs } = global;

storiesOf('Button', module)
  .add('default', () => {
    const story = <Button title="Trop cool ton bouton" className="malade" />;
    const wrapper = mount(story);

    specs(() => describe('Button default', () => {
      it('shows a button', () => {
        expect(wrapper.find('button')).to.have.length(1);
      });

      it('shows the title', () => {
        expect(wrapper.find('.Button').text()).to.equal('Trop cool ton bouton');
      });

      it('has the correct class name', () => {
        expect(wrapper.find('.Button').hasClass('malade')).to.equal(true);
      });

      it('has the `fit` class', () => {
        expect(wrapper.find('.Button').hasClass('fit')).to.equal(true);
      });
    }));

    return story;
  })
  .add('with an href', () => {
    const story = <Button title="Trop cool ton lien" href="http://www.google.com" newWindow />;
    const wrapper = mount(story);

    specs(() => describe('Button with an href', () => {
      it('shows an anchor with the correct href', () => {
        expect(wrapper.find('a').prop('href')).to.equal('http://www.google.com');
      });

      it('opens in a new window', () => {
        expect(wrapper.find('a').prop('target')).to.equal('_blank');
      });
    }));

    return story;
  })
  .add('with full width and primary type', () => {
    const story = <Button title="Trop cool ton lien" href="#" type="primary" width="full" />;
    const wrapper = mount(story);

    specs(() => describe('Button with full width and primary type', () => {
      it('has the `primary` class', () => {
        expect(wrapper.find('.Button').hasClass('primary')).to.equal(true);
      });

      it('has the `full` class', () => {
        expect(wrapper.find('.Button').hasClass('full')).to.equal(true);
      });
    }));

    return story;
  })
  .add('with icons', () => {
    const story = <Button title="Trop cool tes icônes" href="#" iconLeft="multipleDates" iconRight="chevronDown" />;
    const wrapper = mount(story);

    specs(() => describe('Button with icons', () => {
      it('shows two icons', () => {
        expect(wrapper.find('Icon')).to.have.length(2);
      });
    }));

    return story;
  })
  .add('small', () => {
    const story = <Button title="Trop petit ton bouton" small />;
    const wrapper = mount(story);

    specs(() => describe('Button small', () => {
      it('has the correct class name', () => {
        expect(wrapper.find('.Button').hasClass('small')).to.equal(true);
      });
    }));

    return story;
  })
  .add('with a hidden title', () => {
    const story = <Button title="Titre caché" iconLeft="multipleDates" hideTitle />;
    const wrapper = mount(story);

    specs(() => describe('Button with a hidden title', () => {
      it('has an aria-label attribute', () => {
        expect(wrapper.find('.Button').prop('aria-label')).to.equal('Titre caché');
      });

      it('has a title attribute', () => {
        expect(wrapper.find('.Button').prop('title')).to.equal('Titre caché');
      });
    }));

    return story;
  });
