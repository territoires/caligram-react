import React from 'react';
import { mount } from 'enzyme';
import { expect } from 'chai';
import sinon from 'sinon';
import { items, itemsWithConflictingId, otherItems, itemsWithoutGroup } from './fixtures';
import { SelectionList } from '../components';

const { storiesOf, describe, it, specs } = global;

const defaultAction = function (item) {
  const index = this.state.selected.indexOf(item);
  if (index > -1) {
    this.state.selected.splice(index, 1);
    this.setState(prevState => ({
      ...prevState,
      selected: prevState.selected,
    }));
  } else {
    this.state.selected.push(item);
    this.setState(prevState => ({
      ...prevState,
      selected: prevState.selected,
    }));
  }
};

const clear = function () {
  this.state.selected.splice(0, this.state.selected.length);
  return this.setState(prevState => ({
    ...prevState,
    selected: prevState.selected,
  }));
};

storiesOf('SelectionList', module)
  .add('default', () => {
    const story = <SelectionList available={items} labelKey="name" />;
    const wrapper = mount(story);

    specs(() => describe('SelectionList default', () => {
      it('shows a list of items', () => {
        expect(wrapper.find('SelectionList li').length).to.equal(4);
      });

      it('reacts to new props', () => {
        wrapper.setProps({
          available: otherItems,
        });

        expect(wrapper.find('SelectionList li').length).to.equal(3);
      });
    }));

    return story;
  })

  .add('with action', () => {
    const selection = [];
    const spy = sinon.spy(defaultAction);

    const story = <SelectionList available={items} selected={selection} labelKey="name" customClassName="a-custom-class-name" action={spy} />;
    const wrapper = mount(story);

    specs(() => describe('SelectionList with action', () => {
      it('manipulates an intermediate data source', () => {
        wrapper.find('SelectableLabel span').first().simulate('click');
        expect(selection.length).to.equal(1);

        wrapper.find('SelectableLabel span').last().simulate('click');
        expect(selection.length).to.equal(2);

        wrapper.find('SelectableLabel span').first().simulate('click');
        expect(selection.length).to.equal(1);

        wrapper.find('SelectableLabel span').last().simulate('click');
        expect(selection.length).to.equal(0);
      });

      it('connects the same action to all filters', () => {
        expect(wrapper.find('SelectionList li').length).to.equal(4);
        spy.resetHistory();

        expect(spy.callCount).to.equal(0);
        wrapper.find('SelectableLabel span').first().simulate('click');
        expect(spy.callCount).to.equal(1);

        wrapper.find('SelectableLabel span').last().simulate('click');
        expect(spy.callCount).to.equal(2);
      });

      it('supports a provided custom class', () => {
        expect(wrapper.find('ul').hasClass('a-custom-class-name')).to.equal(true);
      });
    }));

    return story;
  })

  .add('using custom keys', () => {
    const customItems = [
      {
        a: 'a name',
        b: 'a group',
        c: 'a-class',
      },
    ];

    const story = <SelectionList available={customItems} labelKey="a" groupKey="b" customClassKey="c" />;
    const wrapper = mount(story);

    specs(() => describe('SelectionList using custom keys', () => {
      it('uses custom keys', () => {
        expect(wrapper.find('li span.group').text()).to.equal('a group');
        expect(wrapper.find('li button').first().text()).to.contain('a name');
        expect(wrapper.find('li').first().hasClass('a-class')).to.equal(true);
      });
    }));

    return story;
  })

  .add('grid', () => {
    const story = <SelectionList available={items} layout="grid" labelKey="name" />;
    const wrapper = mount(story);

    specs(() => describe('SelectionList grid', () => {
      it('has the correct class', () => {
        expect(wrapper.find('ul').first().hasClass('grid')).to.equal(true);
      });
    }));

    return story;
  })

  .add('inline lists', () => {
    const story = (
      <div>
        <SelectionList available={itemsWithoutGroup} labelKey="name" selected={itemsWithoutGroup} layout="inline" />
        <SelectionList available={itemsWithoutGroup} labelKey="name" layout="inline" appearance="tag" />
        <SelectionList available={itemsWithoutGroup} labelKey="name" layout="commaSeparated" appearance="plain" />
      </div>
    );
    const wrapper = mount(story);

    specs(() => describe('SelectionList inline list', () => {
      it('has the correct class', () => {
        expect(wrapper.find('ul').first().hasClass('inline')).to.equal(true);
        expect(wrapper.find('ul').last().hasClass('commaSeparated')).to.equal(true);
      });

      it('passes appearance to the SelectableLabel', () => {
        expect(wrapper.find('ul').at(1).find('SelectableLabel').first().prop('appearance')).to.equal('tag');
      });
    }));

    return story;
  })

  .add('with clear text', () => {
    const localItem = [
      {
        name: 'test',
      },
    ];

    const story = (<SelectionList available={localItem} layout="grid" labelKey="name" clearText="Tous" action={defaultAction} clear={clear} />);
    const wrapper = mount(story);

    specs(() => describe('SelectionList with clear text', () => {
      it('provides a first item used to clear the selection', () => {
        expect(wrapper.find('ul li button').first().text()).to.equal('Tous');
      });

      it('the clear item empties the selection on click', () => {
        expect(wrapper.find('ul li button').first().hasClass('selected')).to.equal(true);
        expect(wrapper.find('ul li button').last().hasClass('selected')).to.equal(false);

        wrapper.find('SelectableLabel button').last().simulate('click');

        expect(wrapper.find('ul li button').first().hasClass('selected')).to.equal(false);
        expect(wrapper.find('ul li button').last().hasClass('selected')).to.equal(true);

        wrapper.find('SelectableLabel button').first().simulate('click');

        expect(wrapper.find('ul li button').first().hasClass('selected')).to.equal(true);
        expect(wrapper.find('ul li button').last().hasClass('selected')).to.equal(false);
      });
    }));

    return story;
  })

  .add('with contextual clear', () => {
    const story = <SelectionList available={items} selected={items} labelKey="name" layout="inline" clear={clear} clearText="Retirer tous les critères" hideClearWhenEmpty />;
    const wrapper = mount(story);

    specs(() => describe('SelectionList with contextual clear', () => {
      it('shows clear button when it has selected items', () => {
        expect(wrapper.find('li.clear').length).to.equal(1);
      });

      it('doesn’t show clear button when it has no selected items', () => {
        wrapper.setProps({ selected: [] });
        expect(wrapper.find('li.clear').length).to.equal(0);
        wrapper.setProps({ selected: items });
      });
    }));

    return story;
  })

  .add('with clear button last', () => {
    const story = (<SelectionList available={items} layout="grid" labelKey="name" clearText="Tous" action={defaultAction} clear={clear} clearPosition="last" />);
    const wrapper = mount(story);

    specs(() => describe('SelectionList with clear button last', () => {
      it('shows clear button at the end of the list', () => {
        expect(wrapper.find('li').last().hasClass('clear')).to.equal(true);
      });
    }));

    return story;
  })

  .add('with custom icons', () => {
    const story = <SelectionList available={items} selected={[items[0]]} labelKey="name" iconSelected="search" iconUnselected="chevronRight" />;
    const wrapper = mount(story);

    specs(() => describe('SelectionList with custom icons', () => {
      it('shows the correct icons', () => {
        expect(wrapper.find('li svg').first().hasClass('search')).to.equal(true);
        expect(wrapper.find('li svg').last().hasClass('chevronRight')).to.equal(true);
      });
    }));

    return story;
  })

  .add('with hybrid selection', () => {
    const story = (
      <SelectionList
        action={defaultAction}
        available={itemsWithConflictingId}
        selected={[itemsWithConflictingId[0]]}
        labelKey="name"
        iconSelected="search"
        iconUnselected="chevronRight"
      />
    );
    const wrapper = mount(story);

    specs(() => describe('SelectionList with custom icons', () => {
      it('only selects one item', () => {
        expect(wrapper.find('button.selected').length).to.equal(1);
      });
    }));

    return story;
  });
