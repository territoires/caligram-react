import React from 'react';
import { shallow } from 'enzyme';
import { expect } from 'chai';
import { event } from './fixtures';
import { EventVenue } from '../components';

const { storiesOf, describe, it, specs } = global;

const { room, venue } = event;
const incompleteVenue = {
  city: 'Montréal',
  longitude: 0,
  latitude: 0,
};

storiesOf('EventVenue', module)
  .add('default', () => {
    const story = <EventVenue venue={venue} />;
    const wrapper = shallow(story);

    specs(() => describe('EventVenue default', () => {
      it('shows the venue name', () => {
        expect(wrapper.find('div.name').text()).to.equal('Maison d’Haïti');
      });

      it("doesn't show the room name", () => {
        expect(wrapper.find('div.room-name')).to.have.length(0);
      });

      it('shows the address', () => {
        const address = wrapper.find('address');
        expect(address.contains('8833, boulevard Saint-Michel')).to.equal(true);
        expect(address.contains('Bureau 601')).to.equal(true);
        expect(address.contains('Montréal (QC) H1Z 3G3')).to.equal(true);
      });

      it('shows a Google Maps link', () => {
        const link = wrapper.find('div.directions a');
        const googleURL = 'http://www.google.com/maps/place/45.57015000,-73.62386600/@45.57015000,-73.62386600,15z';
        expect(link.text()).to.equal('Itinéraire');
        expect(link.prop('href')).to.equal(googleURL);
      });
    }));

    return story;
  })

  .add('with an incomplete venue', () => {
    const story = <EventVenue venue={incompleteVenue} />;
    const wrapper = shallow(story);

    specs(() => describe('EventVenue with an incomplete venue', () => {
      it('shows the city', () => {
        expect(wrapper.find('address').text()).to.equal('Montréal');
      });

      it('doesn’t show a venue name', () => {
        expect(wrapper.find('div.name')).to.have.length(0);
      });

      it('doesn’t show a Google Maps link', () => {
        expect(wrapper.find('div.directions a')).to.have.length(0);
      });
    }));

    return story;
  })

  .add('supports specifying a room', () => {
    const story = <EventVenue venue={venue} room={room} />;
    const wrapper = shallow(story);

    specs(() => describe('EventVenue supports specifying a room', () => {
      it('shows the room name', () => {
        expect(wrapper.find('div.room-name').text()).to.equal('Le gros lampadaire');
      });
    }));

    return story;
  })

  .add('with link on the venue name', () => {
    const story = <EventVenue venue={venue} linkOnVenueName />;
    const wrapper = shallow(story);

    specs(() => describe('EventVenue with link on the venue name', () => {
      it('doesn’t show a Directions link', () => {
        expect(wrapper.find('div.directions a')).to.have.length(0);
      });

      it('has a Google Maps link on the name', () => {
        expect(wrapper.find('div.name a')).to.have.length(1);
      });
    }));

    return story;
  });
