import React from 'react';
import { Col, Grid } from '../components';

const { storiesOf } = global;

const style = {
  backgroundColor: '#eee',
  padding: '2em',
};

storiesOf('Grid', module)
  .add('default', () => {
    return <Grid><div style={style}>yo</div></Grid>;
  })
  .add('with columns', () => {
    return (
      <Grid cols={12}>
        <Col span={1}><div style={style}>1</div></Col>
        <Col span={2}><div style={style}>2 columns</div></Col>
        <Col span={3}><div style={style}>3 columns</div></Col>
        <Col span={6}><div style={style}>6 columns</div></Col>
      </Grid>
    );
  });
