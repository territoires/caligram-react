import React from 'react';
import { mount } from 'enzyme';
import { expect } from 'chai';
import moment from 'moment';
import { Calendar } from '../components';

const { storiesOf, describe, it, specs } = global;

storiesOf('Calendar', module)
  .add('default', () => {
    const noOpFn = () => {};
    const story = <Calendar date={moment()} onSelect={noOpFn} />;
    const wrapper = mount(story);

    specs(() => describe('Calendar default', () => {
      it('shows a calendar', () => {
        expect(wrapper.find('table.Calendar').length).to.equal(1);
      });
    }));

    return story;
  });
