import React from 'react';
import { mount } from 'enzyme';
import { expect } from 'chai';
import { Tab } from '../components';

const { storiesOf, describe, it, specs } = global;

storiesOf('Tab', module)
  .add('default', () => {
    const story = <Tab title="Un onglet" />;
    const wrapper = mount(story);

    specs(() => describe('Tab default', () => {
      it('shows a tab', () => {
        expect(wrapper.find('.Button')).to.have.length(1);
      });
    }));

    return story;
  })

  .add('selected', () => {
    const story = <Tab title="Un onglet sélectionné" selected />;
    const wrapper = mount(story);

    specs(() => describe('Tab selected', () => {
      it('shows a selected tab', () => {
        expect(wrapper.find('.Button').first().hasClass('primary')).to.equal(true);
      });
    }));

    return story;
  })

  .add('small', () => {
    const story = <Tab title="Un petit onglet" small />;
    const wrapper = mount(story);

    specs(() => describe('Tab small', () => {
      it('shows a small tab', () => {
        expect(wrapper.find('.Button').first().hasClass('small')).to.equal(true);
      });
    }));

    return story;
  });
