import React from 'react';
import { mount } from 'enzyme';
import { expect } from 'chai';
import { Checkbox } from '../components';

const { storiesOf, describe, it, specs, action } = global;
const clicked = () => action('Checkbox clicked');

storiesOf('Checkbox', module)
  .add('default', () => {
    const story = <Checkbox title="Trop cool ta case à cocher" id="cool" action={clicked} />;
    const wrapper = mount(story);

    specs(() => describe('Checkbox default', () => {
      it('shows a checkbox', () => {
        expect(wrapper.find('input[type="checkbox"]')).to.have.length(1);
      });
    }));

    return story;
  })

  .add('checked', () => {
    const story = <Checkbox title="Trop cool ta case à cocher" id="cool" checked action={clicked} />;
    const wrapper = mount(story);

    specs(() => describe('Checkbox checked', () => {
      it('shows a checked checkbox', () => {
        expect(wrapper.find('input[checked]')).to.have.length(1);
      });
    }));

    return story;
  });
