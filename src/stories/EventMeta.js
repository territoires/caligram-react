import React from 'react';
import { mount } from 'enzyme';
import { expect } from 'chai';
import DocumentMeta from 'react-document-meta';
import { EventMeta } from '../components';

const { storiesOf, describe, it, specs } = global;

const props = {
  component: DocumentMeta,
  event: {
    title: 'Trop cool ton titre',
    description: '<p>Description avec du <em>formattage fancy</em></p>',
    photo_url: 'http://placeimg.com/256/256/animals',
  },
  siteName: 'Caligram',
  twitterUsername: '@coopterritoires',
};

storiesOf('EventMeta', module)
  .add('default', () => {
    const story = <EventMeta {...props} />;
    const wrapper = mount(story);

    // FIXME: We probably should test that it actually creates <meta> tags, but I’m not sure how

    specs(() => describe('EventMeta default', () => {
      it('shows the description as plain text', () => {
        expect(wrapper.find(DocumentMeta).prop('description')).to.equal('Description avec du formattage fancy');
      });
    }));

    return story;
  });
