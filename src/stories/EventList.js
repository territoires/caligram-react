import React from 'react';
import moment from 'moment';
import { mount, shallow } from 'enzyme';
import { expect } from 'chai';
import { events, filters } from './fixtures';
import { EventList } from '../components';

const { storiesOf, describe, it, specs } = global;

storiesOf('EventList', module)
  .add('default', () => {
    const story = <EventList events={events} />;
    const wrapper = shallow(story);

    specs(() => describe('EventList default', () => {
      it(`renders ${events.length} EventListItem components`, () => {
        expect(wrapper.find('EventListItem')).to.have.length(events.length);
      });

      it('groups by day', () => {
        expect(wrapper.find('time').first().text()).to.not.contain('Semaine du'); // FIXME: Not a great way to test this
      });

      it('doesn’t show pagination', () => {
        expect(wrapper.find('Pagination')).to.have.length(0);
      });

      it('doesn’t show filters', () => {
        expect(wrapper.find('ActiveFilters ul li')).to.have.length(0);
      });

      it('doesn’t have the loading class', () => {
        expect(wrapper.find('.EventList').first().hasClass('loading')).to.equal(false);
      });

      it("doesn't display a title", () => {
        expect(wrapper.find('h2').length).to.equal(0);
      });

      it('doesn’t have EventListItems with the small class', () => {
        expect(wrapper.find('EventListItem').first().hasClass('small')).to.equal(false);
      });
    }));

    return story;
  })

  .add('grouped by week', () => {
    const story = <EventList events={events} groupBy="week" />;
    const wrapper = shallow(story);

    specs(() => describe('EventList grouped by week', () => {
      it('groups by week', () => {
        expect(wrapper.find('time').first().text()).to.contain('Semaine du'); // FIXME: Not a great way to test this
      });
    }));

    return story;
  })

  .add('ungrouped grid', () => {
    const dateFormat = ev => moment(ev.start_date).format('Do MMMM');
    const story = <EventList events={events} groupBy="none" layout="grid" dateFormat={dateFormat} />;
    const wrapper = shallow(story);

    specs(() => describe('EventList ungrouped grid', () => {
      it('doesn’t group', () => {
        expect(wrapper.find('section')).to.have.length(0);
      });
    }));

    return story;
  })

  .add('ungrouped small grid', () => {
    const dateFormat = ev => moment(ev.start_date).format('Do MMMM');
    const story = <EventList events={events} groupBy="none" layout="grid" size="small" dateFormat={dateFormat} />;
    const wrapper = mount(story);

    specs(() => describe('EventList ungrouped small grid', () => {
      it('has EventListItems with the small class', () => {
        expect(wrapper.find('.EventListItem').first().hasClass('small')).to.equal(true);
      });
    }));

    return story;
  })

  .add('ungrouped 2-column grid', () => {
    const story = <EventList events={events} layout="grid" groupBy="none" columns={2} container={4} />;
    const wrapper = shallow(story);

    specs(() => describe('EventList ungrouped 2-column grid', () => {
      it('displays a 2-column grid', () => {
        expect(wrapper.find('div.EventList').first().hasClass('columns-2 container-4')).to.equal(true);
      });
    }));

    return story;
  })

  .add('carousel', () => {
    const story = <EventList events={events} layout="carousel" groupBy="none" />;
    const wrapper = shallow(story);

    specs(() => describe('EventList carousel', () => {
      it('displays a horizontally scrolling carousel', () => {
        expect(wrapper.find('div.EventList').first().hasClass('carousel')).to.equal(true);
      });
    }));

    return story;
  })

  .add('with no events', () => {
    const story = <EventList />;
    const wrapper = shallow(story);

    specs(() => describe('EventList with no events', () => {
      it('shows a default message', () => {
        expect(wrapper.find('p.empty')).to.have.length(1);
      });
    }));

    return story;
  })

  .add('with placeholder image', () => {
    const placeholderImage = 'http://placeimg.com/256/256/animals';
    const message = 'Y’a pas d’événements là tsé';
    const noImageEvents = events.map((event) => {
      return Object.assign({}, event, { photo_url: null });
    });
    const story = (
      <EventList
        events={noImageEvents}
        emptyMessage={message}
        placeholderImage={placeholderImage}
      />
    );
    const wrapper = mount(story);

    specs(() => describe('EventList with placeholder image', () => {
      it('shows the placeholder image', () => {
        expect(wrapper.find('img').first().props().src).to.equal(placeholderImage);
      });
    }));

    return story;
  })

  .add('with pagination', () => {
    const noop = () => {};
    const story = <EventList events={events} currentPage={1} totalPages={3} pageChange={noop} />;
    const wrapper = mount(story);

    specs(() => describe('EventList with pagination', () => {
      it('shows Pagination component', () => {
        expect(wrapper.find('Pagination')).to.have.length(1);
      });
    }));

    return story;
  })

  .add('with filters', () => {
    const story = <EventList events={events} filters={filters} />;
    const wrapper = mount(story);

    specs(() => describe('EventList with filters', () => {
      it('shows ActiveFilters component', () => {
        expect(wrapper.find('ActiveFilters ul li')).to.have.length(filters.length + 1);
      });
    }));

    return story;
  })

  .add('with no events and a custom message', () => {
    const message = 'Y’a pas d’événements là tsé';
    const story = <EventList emptyMessage={message} />;
    const wrapper = shallow(story);

    specs(() => describe('EventList with no events', () => {
      it('shows a default message', () => {
        expect(wrapper.find('p.empty').first().text()).to.equal(message);
      });
    }));

    return story;
  })

  .add('with filters but no events', () => {
    const story = <EventList filters={filters} />;
    const wrapper = mount(story);

    specs(() => describe('EventList with filters but no events', () => {
      it('shows ActiveFilters component', () => {
        expect(wrapper.find('ActiveFilters ul li')).to.have.length(filters.length + 1);
      });
    }));

    return story;
  })

  .add('with a template for items', () => {
    const templateMethod = (event) => {
      return <div>{ event.title }</div>;
    };
    const story = (<EventList events={events} template={templateMethod} />);
    const wrapper = mount(story);

    specs(() => describe('EventList with a template for items', () => {
      it('uses the template', () => {
        expect(wrapper.contains(<div>{ events[0].title }</div>)).to.equal(true);
      });
    }));

    return story;
  })

  .add('loading', () => {
    const story = <EventList events={events} loading />;
    const wrapper = shallow(story);

    specs(() => describe('EventList loading', () => {
      it('has the correct class', () => {
        expect(wrapper.find('.EventList').first().hasClass('loading')).to.equal(true);
      });
    }));

    return story;
  })

  .add('with a custom className', () => {
    const story = <EventList events={events} className="roger" />;
    const wrapper = shallow(story);

    specs(() => describe('EventList with a custom className', () => {
      it('has the correct class', () => {
        expect(wrapper.find('.EventList').first().hasClass('roger')).to.equal(true);
      });
    }));

    return story;
  })

  .add('with a title', () => {
    const story = <EventList events={events} title="En vedette" />;
    const wrapper = shallow(story);

    specs(() => describe('EventList with a custom className', () => {
      it('displays a title', () => {
        expect(wrapper.find('h2').text()).to.equal('En vedette');
      });
    }));

    return story;
  })

  .add('with locale', () => {
    const story = <EventList events={events} locale="en" currentPage={1} totalPages={3} />;
    const wrapper = shallow(story);

    specs(() => describe('EventList with locale', () => {
      it('uses the correct locale', () => {
        expect(wrapper.find('time').first().text()).to.equal('December 17th');
      });
    }));

    return story;
  })

  .add('with inserted content', () => {
    const props = {
      beforeFilters: <p style={{ fontSize: '14px', color: 'red' }}>Cool</p>,
      beforeEvents: <p style={{ fontSize: '14px', color: 'blue' }}>Nice</p>,
      events,
      filters,
    };
    const story = <EventList {...props} />;
    const wrapper = shallow(story);

    specs(() => describe('EventList with inserted content', () => {
      it('displays the inserted content', () => {
        expect(wrapper.find('p').length).to.equal(2);
        expect(wrapper.find('p').first().text()).to.equal('Cool');
      });
    }));

    return story;
  })

  .add('with target blank', () => {
    const story = <EventList events={events} target="_blank" />;
    const wrapper = mount(story);

    specs(() => describe('EventList with target blank', () => {
      it('shows links with target="_blank"', () => {
        const { props: { target } } = wrapper.find('a').get(0);
        expect(target).to.equal('_blank');
      });
    }));

    return story;
  });
