import React from 'react';
import { mount } from 'enzyme';
import { expect } from 'chai';
import { Page } from '../components';

const { storiesOf, describe, it, specs, action } = global;

storiesOf('Page', module)
  .add('default', () => {
    const story = <Page><p>Du contenu sans titre.</p></Page>;
    const wrapper = mount(story);

    specs(() => describe('Page default', () => {
      it('shows children', () => {
        expect(wrapper.find('section.content p').text()).to.equal('Du contenu sans titre.');
      });
    }));

    return story;
  })

  .add('with title', () => {
    const story = <Page title="Trop cool ton titre"><p>Du contenu avec titre.</p></Page>;
    const wrapper = mount(story);

    specs(() => describe('Page with title', () => {
      it('shows the title', () => {
        expect(wrapper.find('header h1').text()).to.equal('Trop cool ton titre');
      });

      it('shows children', () => {
        expect(wrapper.find('section.content p').text()).to.equal('Du contenu avec titre.');
      });
    }));

    return story;
  })

  .add('with subtitle and custom layout', () => {
    const story = (
      <Page title="Trop cool ton titre" subtitle="Pis ton sous-titre itou" layout="custom" className="roger">
        <p style={{ backgroundColor: '#eee', padding: '3em 0', textAlign: 'center', fontSize: 'inherit' }}>Un paragraphe gris pleine largeur.</p>
      </Page>
    );
    const wrapper = mount(story);

    specs(() => describe('Page with subtitle and custom layout', () => {
      it('shows the subtitle', () => {
        expect(wrapper.find('header h2').text()).to.equal('Pis ton sous-titre itou');
      });

      it('doesn’t wrap content in markup', () => {
        expect(wrapper.find('section.content div.body')).to.have.length(0);
      });

      it('has the specified class name', () => {
        expect(wrapper.find('.Page').hasClass('roger')).to.equal(true);
      });
    }));

    return story;
  })

  .add('with back button', () => {
    const story = <Page backButtonAction={() => action('Back')}><p>Du contenu.</p></Page>;
    const wrapper = mount(story);

    specs(() => describe('Page with back button', () => {
      it('shows the back button', () => {
        expect(wrapper.find('.Button').text()).to.equal('Tous les événements');
      });
    }));

    return story;
  });
