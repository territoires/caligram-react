import React from 'react';
import { mount, shallow } from 'enzyme';
import { expect } from 'chai';
import sinon from 'sinon';
import { Pagination } from '../components';

const { storiesOf, describe, it, specs } = global;

const defaultAction = function (page) {
  this.setState({ page });
};
const spy = sinon.spy(defaultAction);

storiesOf('Pagination', module)
  .add('default', () => {
    const story = <Pagination action={spy} currentPage={2} totalPages={3} />;
    const wrapper = mount(story);

    specs(() => describe('Pagination default', () => {
      it('shows buttons', () => {
        expect(wrapper.find('Button').length).to.equal(2);
      });

      it('binds an action to buttons', () => {
        wrapper.find('button.previous').first().simulate('click');
        expect(wrapper.state('page')).to.equal(1);

        wrapper.find('button.next').first().simulate('click');
        expect(wrapper.state('page')).to.equal(3);
      });
    }));

    return story;
  })

  .add('on the first page', () => {
    const story = <Pagination action={spy} currentPage={1} totalPages={3} />;
    const wrapper = shallow(story);

    specs(() => describe('Pagination on the first page', () => {
      it('disables the previous button', () => {
        expect(wrapper.find('Button.previous').first().is('[disabled]')).to.equal(true);
      });
    }));

    return story;
  })

  .add('on the last page', () => {
    const story = <Pagination action={spy} currentPage={3} totalPages={3} />;
    const wrapper = shallow(story);

    specs(() => describe('Pagination on the last page', () => {
      it('disables the next button', () => {
        expect(wrapper.find('Button.next').first().is('[disabled]')).to.equal(true);
      });
    }));

    return story;
  });
