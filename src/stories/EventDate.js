import React from 'react';
import { mount, shallow } from 'enzyme';
import { expect } from 'chai';
import { event } from './fixtures';
import { EventDate, Link } from '../components';

const { storiesOf, describe, it, specs } = global;

const singleDate = event.dates.slice(0, 1);
const singleDateWithEndTime = event.dates.slice(1, 2);
const url = 'http://canonical.url.com/evenements/trop-cool-ton-evenement/1';

storiesOf('EventDate', module)
  .add('with a single date', () => {
    const story = <EventDate dates={singleDate} url={url} />;
    const wrapper = shallow(story);

    specs(() => describe('EventDate with a single date', () => {
      it('shows the date', () => {
        expect(wrapper.find('div.date').text()).to.equal('jeudi 23 juin 2016');
      });

      it('shows the time', () => {
        expect(wrapper.find('div.time').text()).to.equal('15 h');
      });

      it('doesn’t show a date count', () => {
        expect(wrapper.find('span.count')).to.have.length(0);
      });
    }));

    return story;
  })

  .add('with localized date', () => {
    const story = <EventDate locale="en" dates={singleDate} url={url} />;
    const wrapper = shallow(story);

    specs(() => describe('EventDate with localized date', () => {
      it('shows the localized date', () => {
        expect(wrapper.find('div.date').text()).to.equal('Thursday June 23, 2016');
      });
    }));

    return story;
  })

  .add('with time range', () => {
    const story = <EventDate dates={singleDateWithEndTime} url={url} />;
    const wrapper = shallow(story);

    specs(() => describe('EventDate with time range', () => {
      it('shows a time range', () => {
        expect(wrapper.find('div.time').text()).to.equal('15 h à 18 h');
      });
    }));

    return story;
  })

  .add('with multiple dates', () => {
    const story = <EventDate dates={event.dates} url={url} />;
    const wrapper = mount(story);

    specs(() => describe('EventDate with multiple dates', () => {
      it('shows the first date by default', () => {
        expect(wrapper.find('div.date').text()).to.equal('jeudi 23 juin 2016');
      });

      it('shows a date count', () => {
        expect(wrapper.find('button.count').text()).to.contain('3 autres dates');
      });

      it('shows the date range', () => {
        expect(wrapper.find('span.range').text()).to.equal('du 23 juin au 6 août');
      });

      it('shows a menu with all dates', () => {
        expect(wrapper.find('Popover').prop('dropdownView').props.children.length).to.equal(4);
      });

      it('selects the correct date in the menu', () => {
        const popover = shallow(wrapper.find('Popover').prop('dropdownView'));
        expect(popover.find('li').at(0).hasClass('selected')).to.equal(true);
        expect(popover.find('li').at(1).hasClass('selected')).to.equal(false);
      });
    }));

    return story;
  })

  .add('with a selected date', () => {
    const story = <EventDate dates={event.dates} selectedDate={event.dates[1]} url={url} />;
    const wrapper = mount(story);

    specs(() => describe('EventDate with a selected date', () => {
      it('shows the correct date', () => {
        expect(wrapper.find('div.date').text()).to.equal('mercredi 29 juin 2016');
      });

      it('selects the correct date in the menu', () => {
        const popover = shallow(wrapper.find('Popover').prop('dropdownView'));
        expect(popover.find('li').at(0).hasClass('selected')).to.equal(false);
        expect(popover.find('li').at(1).hasClass('selected')).to.equal(true);
      });

      it('shows the correct date when another date is selected', () => {
        const popover = shallow(wrapper.find('Popover').prop('dropdownView'));
        popover.find(Link).at(2).simulate('click');
        expect(wrapper.find('div.date').text()).to.equal('mercredi 6 juillet 2016');
      });
    }));

    return story;
  })

  .add('with an invalid selected date', () => {
    const story = <EventDate dates={event.dates} selectedDate={undefined} url={url} />;
    const wrapper = shallow(story);

    specs(() => describe('EventDate with an invalid selected date', () => {
      it('shows the first date', () => {
        expect(wrapper.find('div.date').text()).to.equal('jeudi 23 juin 2016');
      });
    }));

    return story;
  })

  .add('at midnight with no duration', () => {
    const story = <EventDate dates={event.dates} selectedDate={event.dates[3]} url={url} />;
    const wrapper = shallow(story);

    specs(() => describe('EventDate at midnight with no duration', () => {
      it('doesn’t show a time', () => {
        expect(wrapper.find('div.time')).to.have.length(0);
      });
    }));

    return story;
  })

  .add('with date notes', () => {
    const story = <EventDate dates={event.dates} notes={event.date_notes} />;
    const wrapper = shallow(story);

    specs(() => describe('EventDate with date notes', () => {
      it('shows date notes', () => {
        expect(wrapper.find('div.notes')).to.have.length(1);
      });
    }));

    return story;
  });
