import React from 'react';
import { mount } from 'enzyme';
import { expect } from 'chai';
import sinon from 'sinon';
import { searchSuggestions } from './fixtures';
import { SearchBar } from '../components';

const { storiesOf, describe, it, specs } = global;

storiesOf('SearchBar', module)
  .add('default', () => {
    const properties = {
      buttonText: 'Trouver',
      onChange: () => {},
      onClear: () => {},
      onFetch: () => {},
      onSelect: () => {},
      onSubmit: () => {},
      placeholder: 'Veuillez entrer quelque chose',
      query: 'abc',
      renderSuggestion: () => {},
      suggestions: [],
    };

    const story = (
      <div style={{ margin: '30px auto', maxWidth: '300px' }}>
        <SearchBar {...properties} />
      </div>
    );
    const wrapper = mount(story);

    specs(() => describe('SearchBar default', () => {
      it('shows a search bar', () => {
        expect(wrapper.find('form.SearchBar').length).to.equal(1);
      });

      it('the placeholder can be changed', () => {
        const inputPlaceholder = wrapper.find('form.SearchBar input').first().props().placeholder;
        expect(inputPlaceholder).to.equal(properties.placeholder);
      });

      it('the initial value can be set', () => {
        const inputValue = wrapper.find('form.SearchBar input').first().props().value;
        expect(inputValue).to.equal(properties.query);
      });

      it('the initial value of the button can be set', () => {
        const buttonText = wrapper.find('form.SearchBar button').first().props().children;
        expect(buttonText).to.equal(properties.buttonText);
      });
    }));

    return story;
  })
  .add('with suggestions', () => {
    const onSelect = () => {};
    const spyOnSelect = sinon.spy(onSelect);

    const onFetch = () => {};
    const spyOnFetch = sinon.spy(onFetch);

    const properties = {
      onChange: () => {},
      onClear: () => {},
      onFetch: spyOnFetch,
      onSelect: spyOnSelect,
      onSubmit: () => {},
      renderSuggestion: (suggestion) => {
        return (
          <div data-group={suggestion.group}>
            {suggestion.image && <img src={suggestion.image} alt={suggestion.label} />}
            <span>{suggestion.label}</span>
            {suggestion.venue && <div className="venue">{suggestion.venue}</div>}
          </div>
        );
      },
      minLength: 0,
      suggestions: searchSuggestions,
      query: '',
    };

    const story = (
      <div style={{ margin: '30px auto', maxWidth: '300px' }}>
        <SearchBar {...properties} />
      </div>
    );
    const wrapper = mount(story);

    specs(() => describe('SearchBar behaviour', () => {
      it('changes the content on click and calls onSelect', () => {
        const inputValue = wrapper.find('form.SearchBar input').first().props().value;
        expect(inputValue).to.equal('');

        wrapper.find('li.suggestion > div').first().simulate('click');
        expect(spyOnSelect.callCount).to.equal(1);

        wrapper.find('li.suggestion > div').last().simulate('click');
        expect(spyOnSelect.callCount).to.equal(2);
      });

      it('calls onFetch on input focus', () => {
        // Plugin initialization
        expect(spyOnFetch.callCount).to.equal(2);

        wrapper.find('form.SearchBar input').simulate('focus');
        expect(spyOnFetch.callCount).to.equal(3);
      });
    }));

    return story;
  });
