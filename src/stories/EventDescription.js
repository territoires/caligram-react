import React from 'react';
import { mount } from 'enzyme';
import { expect } from 'chai';
import { EventDescription } from '../components';

const { storiesOf, describe, it, specs } = global;

const description = '<p>Trop cool ta description</p>';

storiesOf('EventDescription', module)
  .add('default', () => {
    const story = <EventDescription description={description} />;
    const wrapper = mount(story);

    specs(() => describe('EventDescription default', () => {
      it('shows a description', () => {
        expect(wrapper.find(EventDescription).render().find('p').text()).to.equal('Trop cool ta description');
      });
    }));

    return story;
  })

  .add('with no description', () => {
    const story = <EventDescription />;
    const wrapper = mount(story);

    specs(() => describe('EventDescription with no description', () => {
      it('doesn’t show a description', () => {
        expect(wrapper.find(EventDescription).render().find('p').length).to.equal(0);
      });
    }));

    return story;
  });
