import React from 'react';
import { mount } from 'enzyme';
import { expect } from 'chai';
import { event } from './fixtures';
import {
  Button,
  EventContact,
  EventDate,
  EventDescription,
  EventDetail,
  EventImage,
  EventPrice,
  EventVenue,
  EventVideo,
  SelectionList,
  ShareLinks,
  StaticMap,
} from '../components';

const { storiesOf, describe, it, specs, action } = global;

storiesOf('EventDetail', module)
  .add('default', () => {
    const actions = {
      typeAction: () => { action('Type clicked')(); },
      tagAction: () => { action('Tag clicked')(); },
      audienceAction: () => { action('Audience clicked')(); },
      groupAction: () => { action('Group clicked')(); },
    };
    const story = <EventDetail event={event} mapApiKey="apiKey" {...actions} />;
    const wrapper = mount(story);

    specs(() => describe('EventDetail default', () => {
      it('shows the title', () => {
        expect(wrapper.contains(<h1>Trop cool ton événement</h1>)).to.equal(true);
      });

      it('shows the types as a SelectionList', () => {
        expect(wrapper.contains(
          <SelectionList available={event.types} labelKey="name" layout="commaSeparated" appearance="plain" action={actions.typeAction} />,
        )).to.equal(true);
      });

      it('shows the correct types', () => {
        event.types.forEach((type) => {
          expect(wrapper.find('.Page > header > h2').contains(type.name)).to.equal(true);
        });
      });

      it('shows the date as an EventDate', () => {
        expect(wrapper.contains(
          <EventDate
            dates={event.dates}
            selectedDate={event.dates[0]}
            url={event.url}
            notes={event.date_notes}
          />,
        )).to.equal(true);
      });

      it('shows the venue as an EventVenue', () => {
        expect(wrapper.contains(
          <EventVenue venue={event.venue} room={event.room} />,
        )).to.equal(true);
      });

      it('shows a map as a StaticMap', () => {
        const map = <StaticMap latitude={event.venue.latitude} longitude={event.venue.longitude} apiKey="apiKey" />;
        expect(wrapper.contains(map)).to.equal(true);
      });

      it('shows the image as an EventImage', () => {
        expect(wrapper.contains(<EventImage src={event.photo_url} />)).to.equal(true);
      });

      it('shows the description as an EventDescription', () => {
        const description = <EventDescription description={event.description} />;
        expect(wrapper.contains(description)).to.equal(true);
      });

      it('shows the tags as a SelectionList', () => {
        expect(wrapper.contains(
          <SelectionList available={event.tags} labelKey="name" layout="inline" appearance="tag" action={actions.tagAction} />,
        )).to.equal(true);
      });

      it('shows the correct tags', () => {
        event.tags.forEach((tag) => {
          expect(wrapper.find('.HeadingBlock.tags').contains(tag.name)).to.equal(true);
        });
      });

      it('shows the video as an EventVideo', () => {
        expect(wrapper.contains(<EventVideo html={event.video.html} />)).to.equal(true);
      });

      it('shows the Facebook link as a Button', () => {
        const props = {
          href: 'https://www.facebook.com/events/12345',
          title: 'Événement Facebook',
          width: 'full',
          newWindow: true,
        };
        expect(wrapper.contains(<Button {...props} />)).to.equal(true);
      });

      it('shows the ticket link as a Button', () => {
        const props = {
          href: 'http://pro.caligram.com',
          title: 'Billets',
          type: 'primary',
          width: 'full',
          newWindow: true,
        };
        expect(wrapper.contains(<Button {...props} />)).to.equal(true);
      });

      it('shows the price as an EventPrice', () => {
        const props = {
          type: event.price_type,
          prices: event.prices,
          notes: event.price_notes,
        };
        expect(wrapper.contains(<EventPrice {...props} />)).to.equal(true);
      });

      it('shows the contact info as an EventContact', () => {
        const props = {
          name: event.contact_name,
          phone: event.contact_phone,
          email: event.contact_email,
          url: event.contact_url,
        };
        expect(wrapper.contains(<EventContact {...props} />)).to.equal(true);
      });

      it('shows the audiences as a SelectionList', () => {
        expect(wrapper.contains(
          <SelectionList available={event.audiences} labelKey="name" appearance="plain" action={actions.audienceAction} />,
        )).to.equal(true);
      });

      it('shows the correct audiences', () => {
        event.audiences.forEach((audience) => {
          expect(wrapper.find('.HeadingBlock.audiences').contains(audience.name)).to.equal(true);
        });
      });

      it('shows groups as a SelectionList', () => {
        expect(wrapper.contains(
          <SelectionList available={event.groups} labelKey="name" appearance="plain" action={actions.groupAction} />,
        )).to.equal(true);
      });

      it('shows the correct groups', () => {
        event.groups.forEach((group) => {
          expect(wrapper.find('.HeadingBlock.groups').contains(group.name)).to.equal(true);
        });
      });

      it('shows sharing links as a ShareLinks', () => {
        expect(wrapper.contains(<ShareLinks event={event} icons={['twitter', 'facebook']} />)).to.equal(true);
      });

      it('shows export links as a ShareLinks', () => {
        expect(wrapper.contains(<ShareLinks event={event} icons={['google', 'ical', 'print']} />)).to.equal(true);
      });
    }));

    return story;
  })
  .add('with modern layout', () => {
    const story = <EventDetail layout="modern" event={event} mapApiKey="apiKey" />;
    const wrapper = mount(story);

    specs(() => describe('EventDetail with modern layout', () => {
      it('uses the correct layout', () => {
        expect(wrapper.find('.Page.EventDetail').hasClass('modern')).to.equal(true);
      });
    }));

    return story;
  })
  .add('corner cases', () => {
    const cornerCasesEvent = Object.assign({}, event);
    cornerCasesEvent.price_notes = null;
    cornerCasesEvent.price_type = null;
    cornerCasesEvent.prices = [];

    const story = <EventDetail event={cornerCasesEvent} mapApiKey="apiKey" />;
    const wrapper = mount(story);

    specs(() => describe('EventDetail corner cases', () => {
      it('doesn\'t show the price if only the currency is given', () => {
        expect(wrapper.contains(<h1>Prix</h1>)).to.equal(false);
      });
    }));

    return story;
  });
