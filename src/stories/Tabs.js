import React from 'react';
import { shallow } from 'enzyme';
import { expect } from 'chai';
import { Tab, Tabs } from '../components';

const { storiesOf, describe, it, specs } = global;

storiesOf('Tabs', module)
  .add('default', () => {
    const story = (
      <Tabs selected="deux">
        <Tab key="un" title="Un onglet" />
        <Tab key="deux" title="Un onglet sélectionné" />
        <Tab key="trois" title="Encore un onglet" />
      </Tabs>
    );
    const wrapper = shallow(story);

    specs(() => describe('Tabs default', () => {
      it('shows a tab list with 3 items', () => {
        expect(wrapper.find('Tab')).to.have.length(3);
      });

      it('selects the second tab', () => {
        expect(wrapper.find('Tab').at(1).prop('selected')).to.equal(true);
      });
    }));

    return story;
  })

  .add('small', () => {
    const story = (
      <Tabs selected="deux" small>
        <Tab key="une" title="Un petit onglet" />
        <Tab key="deux" title="Un petit onglet sélectionné" />
        <Tab key="trois" title="Encore un petit onglet" />
      </Tabs>
    );
    const wrapper = shallow(story);

    specs(() => describe('Tabs small', () => {
      it('shows a tab list with small tabs', () => {
        expect(wrapper.find('Tab').first().prop('small')).to.equal(true);
      });
    }));

    return story;
  });
