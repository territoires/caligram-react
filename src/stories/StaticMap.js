import React from 'react';
import { shallow } from 'enzyme';
import { expect } from 'chai';
import { StaticMap } from '../components';

const { storiesOf, describe, it, specs } = global;

const props = {
  apiKey: 'fakeKey',
  latitude: '45.57015000',
  longitude: '-73.62386600',
};

const noLatLong = {
  apiKey: 'fakeKey',
  latitude: 0,
  longitude: 0,
};

storiesOf('StaticMap', module)
  .add('default', () => {
    const story = <StaticMap {...props} />;
    const wrapper = shallow(story);

    specs(() => describe('StaticMap default', () => {
      it('shows a map', () => {
        expect(wrapper.find('img')).to.have.length(1);
      });

      it('links to Google Maps', () => {
        const googleURL = 'http://www.google.com/maps/place/45.57015000,-73.62386600/@45.57015000,-73.62386600,15z';
        expect(wrapper.find('a').first().prop('href')).to.equal(googleURL);
      });

      /* FIXME: Not sure how to test this
      it('includes a custom SVG marker', () => {
        expect(wrapper.find('svg')).to.have.length(1);
      });
      */
    }));

    return story;
  })

  .add('without coordinates', () => {
    const story = <StaticMap {...noLatLong} />;
    const wrapper = shallow(story);

    specs(() => describe('StaticMap without coordinates', () => {
      it('doesn’t show a map', () => {
        expect(wrapper.find('img')).to.have.length(0);
      });

      it('doesn’t show a link', () => {
        expect(wrapper.find('a')).to.have.length(0);
      });
    }));

    return story;
  });
