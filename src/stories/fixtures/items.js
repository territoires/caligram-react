export const items = [
  {
    id: 1,
    name: 'Test',
    group: 'One',
    selected: false,
  },
  {
    id: 2,
    name: 'Test 2',
    group: 'Deux',
    selected: false,
  },
  {
    id: 3,
    name: 'Test 3',
    group: 'One',
    selected: false,
  },
  {
    id: 4,
    name: 'Test 4',
    group: 'One',
    selected: false,
  },
];

export const itemsWithConflictingId = [
  {
    id: 1,
    name: 'Test',
    group: 'One',
    selected: false,
  },
  {
    id: 1,
    name: 'Test 2',
    group: 'Deux',
    selected: false,
  },
  {
    id: 3,
    name: 'Test 3',
    group: 'One',
    selected: false,
  },
  {
    id: 4,
    name: 'Test 4',
    group: 'One',
    selected: false,
  },
];

export const otherItems = [
  {
    id: 1,
    name: 'Autre Test',
    group: 'One',
    selected: false,
  },
  {
    id: 2,
    name: 'Autre Test 2',
    group: 'Deux',
    selected: false,
  },
  {
    id: 3,
    name: 'Autre Test 3',
    group: 'One',
    selected: false,
  },
];

export const itemsWithoutGroup = [
  {
    id: 1,
    name: 'Test',
  },
  {
    id: 2,
    name: 'Test 2',
  },
  {
    id: 3,
    name: 'Test 3',
  },
  {
    id: 4,
    name: 'Test 4',
  },
];

export const filters = [
  {
    id: 1,
    label: 'Filtre 1',
    group: 'One',
    selected: false,
  },
  {
    id: 2,
    label: 'Filtre 2',
    group: 'Deux',
    selected: false,
  },
  {
    id: 3,
    label: 'Filtre 3',
    group: 'One',
    selected: false,
  },
];
