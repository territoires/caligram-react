const searchSuggestions = [
  {
    meta: {
      type: 'events',
    },
    data: [
      {
        id: 1,
        title: 'Test',
        group: 'Événements',
        label: 'Un gros événement',
        image: 'http://placeimg.com/256/256/animals',
        venue: 'Lieu',
      },
      {
        id: 2,
        title: 'Autre Test',
        group: 'Événements',
        label: 'Exposition de chats',
        image: 'http://placeimg.com/256/256/animals',
        venue: 'Autre lieu',
      },
    ],
  },
  {
    meta: {
      type: 'things',
    },
    data: [
      {
        id: 3,
        title: 'Une chose',
        group: 'Choses',
        label: 'Libellé',
      },
    ],
  },
];

export default searchSuggestions;
