import images from './images';

const event = {
  id: 1,
  start_date: '2016-12-17 14:00:00',
  next_date: '2018-12-17 14:00:00',
  title: 'Trop cool ton événement',
  slug: 'trop-cool-ton-evenement',
  dates: [
    {
      id: 50,
      start_date: '2016-06-23 15:00:00',
    },
    {
      id: 51,
      start_date: '2016-06-29 15:00:00',
      duration: 10800,
    },
    {
      id: 52,
      start_date: '2016-07-06 15:00:00',
      duration: 10800,
    },
    {
      id: 53,
      start_date: '2016-08-06 00:00:00',
      duration: null,
    },
  ],
  date_notes: '<p>Horaire sujet à changements selon la météo</p>',
  description: '<p>Trop cool ta <strong>description</strong>.</p><p>Un <a href="#">lien</a>, un boute en <em>italique</em>.<ul><li>Une liste</li><li>C’est fou une liste</li></ul><ol><li>Une liste numérique</li><li>Ben oui</li></ol><p>Yo.</p>',
  images,
  photo_url: 'http://placeimg.com/256/256/animals',
  thumbnail_url: 'http://placeimg.com/64/64/animals',
  display_image_url: 'http://placeimg.com/64/64/animals',
  url: 'http://canonical.url.com/evenements/trop-cool-ton-evenement/1',
  facebook_url: 'https://www.facebook.com/events/12345',
  audiences: [
    {
      id: 1,
      name: 'Humains',
      slug: 'humains',
    },
    {
      id: 2,
      name: 'Tubercules',
      slug: 'tubercules',
    },
  ],
  groups: [
    {
      id: 1,
      inherited: false,
      logo_url: null,
      name: 'Département d’études littéraires',
      role: null,
      slug: 'departement-d-etudes-litteraires',
    },
    {
      id: 2,
      inherited: false,
      logo_url: null,
      name: 'Une autre organisation',
      role: null,
      slug: 'une-autre-organisation',
    },
  ],
  tags: [
    {
      id: 1,
      parent_id: null,
      name: 'Patate chaude',
      slug: 'patate-chaude',
    },
    {
      id: 2,
      parent_id: null,
      name: 'Patate froide',
      slug: 'patate-froide',
    },
  ],
  types: [
    {
      id: 1,
      parent_id: null,
      name: 'Patate chaude',
      slug: 'patate-chaude',
    },
    {
      id: 2,
      parent_id: null,
      name: 'Conférence',
      slug: 'conference',
    },
  ],
  venue: {
    id: 1789,
    parent_id: null,
    name: 'Maison d’Haïti',
    slug: 'maison-d-haiti',
    address: '8833, boulevard Saint-Michel',
    address2: 'Bureau 601',
    city: 'Montréal',
    state: 'QC',
    country: 'Canada',
    zip: 'H1Z 3G3',
    latitude: '45.57015000',
    longitude: '-73.62386600',
  },
  room: {
    id: 290,
    parent_id: null,
    name: 'Le gros lampadaire',
    slug: 'le-gros-lampadaire',
  },
  price_notes: 'Note de prix',
  price_type: 'FIXED',
  price_currency: 'CAD',
  prices: [
    {
      id: 1,
      value: '19.99',
      description: 'Trop cool ton prix',
    },
    {
      id: 2,
      value: '0.99',
      description: 'Trop pas cher ton prix',
    },
  ],
  ticket_url: 'http://pro.caligram.com',
  contact_name: 'Roger Lemuscadet',
  contact_email: 'roger@muscadet.com',
  video: {
    html: "<object width='425' height='344'><param name='movie' value='http://www.youtube.com/v/TnZhi5gaX8g'></param><param name='allowFullScreen' value='true'></param><param name='allowscriptaccess' value='always'></param><embed src='http://www.youtube.com/v/TnZhi5gaX8g' type='application/x-shockwave-flash' width='425' height='344' allowscriptaccess='always' allowfullscreen='true'></embed></object>",
  },
};

export default event;
