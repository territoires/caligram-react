import React from 'react';
import { mount } from 'enzyme';
import { DynamicMap, MapMarker } from '../components';
import { events } from './fixtures';

const { storiesOf, describe, specs } = global;

const handleOnClick = e => e;
const onCenterChange = obj => obj;

const props = {
  apiKey: '',
  centerLat: 45.562371,
  centerLng: -73.600579,
  events,
  handleOnClick,
  initialZoom: 16,
  onCenterChange,
};

storiesOf('DynamicMap', module)
  .add('default', () => {
    const story = (
      <DynamicMap {...props} />
    );

    mount(story);

    specs(() => describe('DynamicMap default', () => {
      // TODO : No tests : it doesn't work, I'll come back to it later
    }));

    return story;
  })
  .add('with MapMarker', () => {
    const extProps = {
      marker: MapMarker,
      offset: {
        top: -15,
        left: -1,
      },
      ...props,
    };

    const story = (
      <DynamicMap {...extProps} />
    );

    mount(story);

    specs(() => describe('DynamicMap with MapMarker', () => {}));

    return story;
  });
