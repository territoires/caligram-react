import React from 'react';
import { shallow } from 'enzyme';
import { expect } from 'chai';
import { EventImage } from '../components';

const { storiesOf, describe, it, specs } = global;

const src = 'http://placeimg.com/256/256/animals';

storiesOf('EventImage', module)
  .add('default', () => {
    const story = <EventImage src={src} />;
    const wrapper = shallow(story);

    specs(() => describe('EventImage default', () => {
      it('shows an image', () => {
        expect(wrapper.contains(<img src={src} alt="" />)).to.equal(true);
      });
    }));

    return story;
  })

  .add('with credit', () => {
    const credit = '© 2019 Roger deux muscadets';
    const story = <EventImage src={src} credit={credit} />;
    const wrapper = shallow(story);

    specs(() => describe('EventImage with credit', () => {
      it('shows credit', () => {
        expect(wrapper.contains(<div className="credit">{credit}</div>)).to.equal(true);
      });
    }));

    return story;
  })

  .add('with no image', () => {
    const story = <EventImage />;
    const wrapper = shallow(story);

    specs(() => describe('EventImage with no image', () => {
      it('doesn’t show an image', () => {
        expect(wrapper.contains(<img src={src} alt="" />)).to.equal(false);
      });
    }));

    return story;
  });
