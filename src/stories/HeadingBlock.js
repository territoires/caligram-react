import React from 'react';
import { mount } from 'enzyme';
import { expect } from 'chai';
import { HeadingBlock } from '../components';

const { storiesOf, describe, it, specs } = global;

storiesOf('HeadingBlock', module)
  .add('default', () => {
    const story = <HeadingBlock title="Malade" className="malade"><div>Oh yeah</div></HeadingBlock>;
    const wrapper = mount(story);

    specs(() => describe('HeadingBlock default', () => {
      it('shows the title', () => {
        expect(wrapper.find('h1').text()).to.equal('Malade');
      });

      it('shows children', () => {
        expect(wrapper.find('div').text()).to.equal('Oh yeah');
      });

      it('has the correct class name', () => {
        expect(wrapper.find('.HeadingBlock').hasClass('malade')).to.equal(true);
      });
    }));

    return story;
  })
  .add('with icon', () => {
    const story = <HeadingBlock title="Malade ton icône" icon="multipleDates"><div>Oh yeah</div></HeadingBlock>;
    const wrapper = mount(story);

    specs(() => describe('HeadingBlock with icon', () => {
      it('shows an icon', () => {
        expect(wrapper.find('Icon')).to.have.length(1);
      });
    }));

    return story;
  })
  .add('inline', () => {
    const story = <HeadingBlock title="Malade ton entête inline" inline><div>Oh yeah</div></HeadingBlock>;
    const wrapper = mount(story);

    specs(() => describe('HeadingBlock inline', () => {
      it('has the inline class', () => {
        expect(wrapper.find('.HeadingBlock').hasClass('inline')).to.equal(true);
      });
    }));

    return story;
  });
