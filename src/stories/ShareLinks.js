import React from 'react';
import { mount } from 'enzyme';
import { expect } from 'chai';
import { ShareLinks } from '../components';
import { event } from './fixtures';

const { storiesOf, describe, it, specs } = global;

storiesOf('ShareLinks', module)
  .add('default', () => {
    const story = <ShareLinks event={event} />;
    const wrapper = mount(story);

    specs(() => describe('ShareLinks default', () => {
      it('shows 6 links', () => {
        expect(wrapper.find('li')).to.have.length(6);
      });
    }));

    return story;
  })
  .add('with fewer icons', () => {
    const story = <ShareLinks event={event} icons={['linkedIn', 'yahoo']} />;
    const wrapper = mount(story);

    specs(() => describe('ShareLinks with fewer icons', () => {
      it('shows 2 links', () => {
        expect(wrapper.find('li')).to.have.length(2);
      });
    }));

    return story;
  })
  .add('without icon tinting', () => {
    const story = <ShareLinks event={event} noTint />;
    const wrapper = mount(story);

    specs(() => describe('ShareLinks without icon tinting', () => {
      it('has the correct class', () => {
        expect(wrapper.find('.ShareLinks').hasClass('noTint')).to.equal(true);
      });
    }));

    return story;
  })
  .add('with no URL', () => {
    const eventWithoutUrl = Object.assign({}, event);
    eventWithoutUrl.url = undefined;
    const story = <ShareLinks event={eventWithoutUrl} />;
    const wrapper = mount(story);

    specs(() => describe('ShareLinks with no URL', () => {
      it('renders nothing', () => {
        expect(wrapper.find('.ShareLinks')).to.have.length(0);
      });
    }));

    return story;
  })
  .add('with an invalid URL', () => {
    const eventWithInvalidUrl = Object.assign({}, event);
    eventWithInvalidUrl.url = 'www.example.com'; // Missing http://
    const story = <ShareLinks event={eventWithInvalidUrl} />;
    const wrapper = mount(story);

    specs(() => describe('ShareLinks with an invalid URL', () => {
      it('shows 6 links', () => {
        expect(wrapper.find('li')).to.have.length(6);
      });
    }));

    return story;
  });
