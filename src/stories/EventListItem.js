import React from 'react';
import moment from 'moment';
import { mount, shallow } from 'enzyme';
import { expect } from 'chai';
import { event } from './fixtures';
import { EventListItem } from '../components';

const { storiesOf, describe, it, specs } = global;

storiesOf('EventListItem', module)
  .add('default', () => {
    const story = <EventListItem event={event} />;
    const wrapper = mount(story);

    specs(() => describe('EventListItem default', () => {
      it('shows the given title', () => {
        expect(wrapper.contains(<h1>Trop cool ton événement</h1>)).to.equal(true);
      });

      it('shows the time', () => {
        expect(wrapper.find('time').first().text()).to.contain('14 h 00');
      });

      it('shows the image', () => {
        expect(wrapper.find('img').length).to.equal(1);
        expect(wrapper.find('img').first().props().src).to.equal(event.display_image_url);
      });

      it('doesn’t have the small class', () => {
        expect(wrapper.find('.EventListItem').hasClass('small')).to.equal(false);
      });
    }));

    return story;
  })

  .add('with no image', () => {
    const noImageEvent = Object.assign({}, event, { display_image_url: null });
    const story = <EventListItem event={noImageEvent} />;
    const wrapper = mount(story);

    specs(() => describe('EventListItem with no image', () => {
      it("doesn't show the image", () => {
        expect(wrapper.find('img').length).to.equal(0);
      });
    }));

    return story;
  })

  .add('with placeholder image', () => {
    const placeholderImage = 'http://placeimg.com/256/256/animals';
    const placeholderImageEvent = Object.assign({}, event, { display_image_url: null });
    const story = (
      <EventListItem
        event={placeholderImageEvent}
        placeholderImage={placeholderImage}
      />
    );
    const wrapper = mount(story);

    specs(() => describe('EventListItem with placeholder image', () => {
      it('shows the placeholder image', () => {
        expect(wrapper.find('img').first().props().src).to.equal(placeholderImage);
      });
    }));

    return story;
  })

  .add('grid with date', () => {
    const customDateFormat = (ev) => {
      return (<span>{moment(ev.start_date).format('Do MMMM')}</span>);
    };
    const story = <EventListItem event={event} layout="grid" dateFormat={customDateFormat} />;
    const wrapper = shallow(story);

    specs(() => describe('EventListItem grid with date', () => {
      it('shows the date in the correct format', () => {
        expect(wrapper.find('time').first().text()).to.contain('17 décembre');
      });
    }));

    return story;
  })

  .add('grid with custom date markup', () => {
    const customDateFormat = (ev) => {
      return (
        <span>
          <span>{moment(ev.start_date).format('D')}</span>
          <div>{moment(ev.start_date).format('MMM')}</div>
        </span>
      );
    };
    const story = <EventListItem event={event} layout="grid" dateFormat={customDateFormat} />;
    const wrapper = shallow(story);

    specs(() => describe('EventListItem grid with custom date markup', () => {
      it('shows the date in custom HTML', () => {
        expect(wrapper.render().find('time > *')).to.have.length(1);
        expect(wrapper.render().find('time > span > span')).to.have.length(1);
        expect(wrapper.render().find('time > span > div')).to.have.length(1);
      });
    }));

    return story;
  })

  .add('small grid', () => {
    const story = <EventListItem event={event} layout="grid" size="small" />;
    const wrapper = shallow(story);

    specs(() => describe('EventListItem small grid', () => {
      it('has the correct class', () => {
        expect(wrapper.find('.EventListItem').hasClass('small')).to.equal(true);
      });
    }));

    return story;
  })

  .add('with other link date', () => {
    const story = <EventListItem event={event} linkDate="next_date" />;
    const wrapper = mount(story);

    specs(() => describe('EventListItem with other link date', () => {
      it('shows the requested date', () => {
        const { props: { href } } = wrapper.find('a').get(0);
        expect(href).to.equal('/evenements/trop-cool-ton-evenement/1?date=2018-12-17_14-00-00');
      });
    }));

    return story;
  })

  .add('with target blank', () => {
    const story = <EventListItem event={event} target="_blank" />;
    const wrapper = mount(story);

    specs(() => describe('EventListItem with target blank', () => {
      it('shows a link with target="_blank"', () => {
        const { props: { target } } = wrapper.find('a').get(0);
        expect(target).to.equal('_blank');
      });
    }));

    return story;
  })

  .add('custom', () => {
    const story = (
      <EventListItem event={event}>
        <div>{event.title}</div>
      </EventListItem>
    );
    /*
     * Missing test
     * const wrapper = shallow(story);
     */

    return story;
  });
