import React from 'react';
import { shallow } from 'enzyme';
import { expect } from 'chai';
import { event } from './fixtures';
import { EventPrice } from '../components';

const { storiesOf, describe, it, specs } = global;

const { prices } = event;

storiesOf('EventPrice', module)
  .add('with free event', () => {
    const story = <EventPrice type="FREE" />;
    const wrapper = shallow(story);

    specs(() => describe('EventPrice with free event', () => {
      it('shows the Free label', () => {
        expect(wrapper.contains('Gratuit')).to.equal(true);
      });
    }));

    return story;
  })

  .add('with free event and a price list', () => {
    const story = <EventPrice type="FREE" prices={prices} />;
    const wrapper = shallow(story);

    specs(() => describe('EventPrice with free event and a price list', () => {
      it('doesn’t show prices', () => {
        expect(wrapper.contains('Trop cool ton prix')).to.equal(false);
      });
    }));

    return story;
  })

  .add('with a price list and notes', () => {
    const story = <EventPrice type="FIXED" prices={prices} notes="J’capote sur tes notes" />;
    const wrapper = shallow(story);

    specs(() => describe('EventPrice with a price list and notes', () => {
      it('shows a price list', () => {
        expect(wrapper.contains('Trop cool ton prix')).to.equal(true);
        expect(wrapper.contains('0.99')).to.equal(true);
      });

      it('sorts the price list by price', () => {
        expect(wrapper.find('ul.prices li').first().contains('0.99')).to.equal(true);
      });

      it('doesn’t show the currency', () => {
        expect(wrapper.contains('CAD')).to.equal(false);
      });

      it('shows notes', () => {
        expect(wrapper.contains('J’capote sur tes notes')).to.equal(true);
      });
    }));

    return story;
  })

  .add('with a specified currency', () => {
    const story = <EventPrice type="FIXED" currency="CAD" prices={prices} />;
    const wrapper = shallow(story);

    specs(() => describe('EventPrice with a price list', () => {
      it('shows the currency', () => {
        expect(wrapper.contains('CAD')).to.equal(true);
      });
    }));

    return story;
  })

  .add('with fixed price and no price list', () => {
    const story = <EventPrice type="FIXED" />;
    const wrapper = shallow(story);

    specs(() => describe('with fixed price and no price list', () => {
      it('shows nothing', () => {
        expect(wrapper.find('.EventPrice').exists()).to.equal(false);
      });
    }));

    return story;
  });
