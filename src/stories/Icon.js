import React from 'react';
import { mount } from 'enzyme';
import { expect } from 'chai';
import { Icon } from '../components';
import CaligramReact from '../config';
import star from './fixtures/star.svg';

const { storiesOf, describe, it, specs } = global;

storiesOf('Icon', module)
  .add('default', () => {
    const story = <Icon name="multipleDates" />;
    const wrapper = mount(story);

    specs(() => describe('Icon default', () => {
      it('shows an SVG', () => {
        expect(wrapper.find('svg')).to.have.length(1);
      });
    }));

    return story;
  })
  .add('with custom icon', () => {
    const config = {
      icons: { star },
    };
    CaligramReact.config(config);
    const story = <Icon name="star" />;
    const wrapper = mount(story);

    specs(() => describe('Icon with custom icon', () => {
      it('shows an SVG', () => {
        expect(wrapper.find('svg')).to.have.length(1);
      });
    }));

    return story;
  });
