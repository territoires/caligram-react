import React from 'react';
import { events } from './fixtures';
import { MapMarker } from '../components';

const { storiesOf } = global;

storiesOf('MapMarker', module)
  .add('default', () => {
    return <div style={{ marginTop: '50px' }}><MapMarker /></div>;
  })

  .add('with a single event', () => {
    return (
      <div style={{ marginTop: '50px' }}>
        <MapMarker {...events[0]} />
      </div>
    );
  })

  .add('with multiple events', () => {
    return (
      <div style={{ marginTop: '50px' }}>
        <MapMarker events={events} />
      </div>
    );
  });
