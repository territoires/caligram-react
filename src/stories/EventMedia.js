import React from 'react';
import { shallow } from 'enzyme';
import { expect } from 'chai';
import { EventMedia } from '../components';
import images from './fixtures/images';

const { storiesOf, describe, it, specs } = global;

storiesOf('EventMedia', module)
  .add('default', () => {
    const story = <EventMedia images={images} />;
    const wrapper = shallow(story);

    specs(() => describe('EventMedia default', () => {
      it('shows multiple images', () => {
        expect(wrapper.find('EventImage')).to.have.lengthOf(images.length);
      });
    }));

    return story;
  })

  .add('with carousel', () => {
    const story = <EventMedia images={images} layout="carousel" />;
    const wrapper = shallow(story);

    specs(() => describe('EventMedia with carousel', () => {
      it('shows thumbnails', () => {
        expect(wrapper.find('nav')).to.have.lengthOf(1);
      });

      it('changes the current image when clicking a thumbnail', () => {
        expect(wrapper.find('button').at(1).hasClass('current')).to.equal(false);
        wrapper.find('button').at(1).simulate('click');
        expect(wrapper.find('button').at(1).hasClass('current')).to.equal(true);
      });
    }));

    return story;
  })

  .add('with a single-image carousel', () => {
    const story = <EventMedia images={images.slice(0, 1)} layout="carousel" />;
    const wrapper = shallow(story);

    specs(() => describe('EventMedia with a single-image carousel', () => {
      it('shows a single EventImage', () => {
        expect(wrapper.find('EventImage')).to.have.lengthOf(1);
      });

      it('doesn’t show thumbnails', () => {
        expect(wrapper.find('nav')).to.have.lengthOf(0);
      });
    }));

    return story;
  });
