import React from 'react';
import { mount } from 'enzyme';
import { expect } from 'chai';
import { filters } from './fixtures';
import { ActiveFilters } from '../components';

const { storiesOf, describe, it, specs } = global;

const defaultAction = function (item) {
  const index = this.state.selected.indexOf(item);
  if (index > -1) {
    this.state.selected.splice(index, 1);
    this.setState(prevState => ({
      ...prevState,
      selected: prevState.selected,
    }));
  } else {
    this.state.selected.push(item);
    this.setState(prevState => ({
      ...prevState,
      selected: prevState.selected,
    }));
  }
};

const clear = function () {
  this.state.selected.splice(0, this.state.selected.length);
  return this.setState(prevState => ({
    ...prevState,
    selected: prevState.selected,
  }));
};

storiesOf('ActiveFilters', module)
  .add('default', () => {
    const story = <ActiveFilters selected={filters} action={defaultAction} clear={clear} groupKey="group" />;
    const wrapper = mount(story);

    specs(() => describe('ActiveFilters default', () => {
      it('shows a SelectionList with the correct class name', () => {
        expect(wrapper.find('.SelectionList').hasClass('ActiveFilters')).to.equal(true);
      });
    }));

    return story;
  });
