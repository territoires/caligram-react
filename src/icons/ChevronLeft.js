import React from 'react';

const SvgChevronLeft = props => (
  <svg width={18} height={18} viewBox="0 0 18 18" {...props}>
    <path d="M10.626 11.563v2.617L6 9.09 10.626 4v2.617L8.378 9.09z" />
  </svg>
);

export default SvgChevronLeft;
