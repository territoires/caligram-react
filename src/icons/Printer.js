import React from 'react';

const SvgPrinter = props => (
  <svg width={18} height={18} viewBox="0 0 18 18" {...props}>
    <g fill="#000" fillRule="evenodd">
      <path d="M14 12v4H4v-4H1V5h16v7h-3zM6 9v5h6V9H6z" />
      <path fillRule="nonzero" d="M4 2h10v2H4z" />
    </g>
  </svg>
);

export default SvgPrinter;
