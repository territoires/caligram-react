import React from 'react';

const SvgList = props => (
  <svg width={18} height={18} viewBox="0 0 18 18" {...props}>
    <g fillRule="nonzero" fill="#000">
      <path d="M2 6h14V4H2zM2 10h14V8H2zM2 14h14v-2H2z" />
    </g>
  </svg>
);

export default SvgList;
