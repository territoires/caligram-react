import React from 'react';

const SvgFacebook = props => (
  <svg width={18} height={18} viewBox="0 0 18 18" {...props}>
    <path d="M14.444 2H3.556C2.7 2 2 2.7 2 3.556v10.888C2 15.301 2.7 16 3.556 16H9v-5.444H7.444V8.63H9V7.036c0-1.683.943-2.865 2.93-2.865l1.401.001v2.026h-.93c-.774 0-1.068.58-1.068 1.119V8.63h1.998l-.442 1.925h-1.556V16h3.111C15.3 16 16 15.3 16 14.444V3.556C16 2.7 15.3 2 14.444 2z" />
  </svg>
);

export default SvgFacebook;
