import React from 'react';

const SvgChevronRight = props => (
  <svg width={18} height={18} viewBox="0 0 18 18" {...props}>
    <path d="M7 11.563v2.617l4.626-5.09L7 4v2.617L9.248 9.09z" />
  </svg>
);

export default SvgChevronRight;
