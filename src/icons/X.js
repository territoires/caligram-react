import React from 'react';

const SvgX = props => (
  <svg width={18} height={18} viewBox="0 0 18 18" {...props}>
    <path d="M9.227 7.813L5.684 4.27 4.27 5.684l3.543 3.543L4.27 12.77l1.414 1.414 3.543-3.543 3.543 3.543 1.414-1.414-3.543-3.543 3.543-3.543L12.77 4.27 9.227 7.813z" />
  </svg>
);

export default SvgX;
