import React from 'react';

const SvgChevronUp = props => (
  <svg width={18} height={18} viewBox="0 0 18 18" {...props}>
    <path d="M11.563 10.626h2.617L9.09 6 4 10.626h2.617L9.09 8.378z" />
  </svg>
);

export default SvgChevronUp;
