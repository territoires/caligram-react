import React from 'react';

const SvgGoogle = props => (
  <svg width={18} height={18} viewBox="0 0 18 18" {...props}>
    <path
      d="M9 8.143V10.2h3.403c-.138.883-1.028 2.587-3.403 2.587-2.048 0-3.72-1.695-3.72-3.787 0-2.093 1.673-3.787 3.72-3.787 1.165 0 1.945.494 2.393.925l1.627-1.57C11.975 3.594 10.62 3 9 3 5.683 3 3 5.683 3 9c0 3.318 2.683 6 6 6 3.463 0 5.76-2.434 5.76-5.863 0-.394-.043-.695-.095-.995H9z"
      fill="#000"
      fillRule="nonzero"
    />
  </svg>
);

export default SvgGoogle;
