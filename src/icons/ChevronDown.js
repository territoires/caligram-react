import React from 'react';

const SvgChevronDown = props => (
  <svg width={18} height={18} viewBox="0 0 18 18" {...props}>
    <path d="M11.563 7h2.617l-5.09 4.626L4 7h2.617L9.09 9.248z" />
  </svg>
);

export default SvgChevronDown;
