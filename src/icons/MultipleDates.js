import React from 'react';

const SvgMultipleDates = props => (
  <svg width={18} height={18} viewBox="0 0 18 18" {...props}>
    <path
      d="M11 5H3v5H1V3h10v2zm3 3H6v6H4V6h10v2zm3 1v7H7V9h10z"
      fillRule="evenodd"
    />
  </svg>
);

export default SvgMultipleDates;
