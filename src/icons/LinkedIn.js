import React from 'react';

const SvgLinkedIn = props => (
  <svg width={18} height={18} viewBox="0 0 18 18" {...props}>
    <path
      d="M13.927 13.93h-2.073v-3.248c0-.775-.015-1.772-1.08-1.772-1.081 0-1.246.843-1.246 1.715v3.305H7.455V7.25h1.991v.91h.027c.278-.524.955-1.079 1.966-1.079 2.1 0 2.489 1.383 2.489 3.183v3.666zM5.113 6.336c-.667 0-1.203-.54-1.203-1.205a1.203 1.203 0 1 1 2.407 0c0 .665-.54 1.205-1.204 1.205zm1.04 7.594h-2.08V7.25h2.08v6.68zM14.965 2H3.033A1.02 1.02 0 0 0 2 3.009V14.99A1.02 1.02 0 0 0 3.033 16h11.93c.57 0 1.037-.45 1.037-1.009V3.01C16 2.452 15.533 2 14.963 2h.002z"
      fill="#000"
      fillRule="nonzero"
    />
  </svg>
);

export default SvgLinkedIn;
