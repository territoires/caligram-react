import React from 'react';

const SvgYahoo = props => (
  <svg width={18} height={18} viewBox="0 0 18 18" {...props}>
    <g transform="translate(3 3)" fill="#000" fillRule="nonzero">
      <ellipse cx={9.429} cy={11.178} rx={1} ry={1} />
      <path d="M10.02 0L5.79 6.813l.082 5.023c-.616-.156-1.19-.156-1.724 0l.123-5.023L0 0l.205.078a3.517 3.517 0 0 0 1.56 0h.124C2.341.934 5.01 5.1 5.01 5.1L8.172.078h.123C9.158.31 10.02 0 10.02 0zm.041 9.539l-.287-.117-.329.039.904-8.488c.164-1.401 1.97-.817 1.601.234l-1.889 8.332z" />
    </g>
  </svg>
);

export default SvgYahoo;
