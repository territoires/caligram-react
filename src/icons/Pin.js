import React from 'react';

const SvgPin = props => (
  <svg width={24} height={36} viewBox="0 0 24 36" {...props}>
    <path
      d="M21.74 11.897C21.74 6.431 17.344 2 11.92 2 6.497 2 2.1 6.431 2.1 11.897s9.82 20.942 9.82 20.942 9.82-15.476 9.82-20.942"
      stroke="#FFF"
      strokeWidth={3}
      fill="#00"
      fillRule="evenodd"
    />
  </svg>
);

export default SvgPin;
