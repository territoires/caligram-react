import React from 'react';

const SvgCheckmark = props => (
  <svg width={18} height={18} viewBox="0 0 18 18" {...props}>
    <path d="M5.477 8.495L4 9.845l3.948 4.322 6.596-8.983L12.932 4 7.78 11.017z" />
  </svg>
);

export default SvgCheckmark;
