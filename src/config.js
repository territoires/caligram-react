import { deepMerge } from './helpers/utils';
import { fr, en } from './locales';
import checkmark from './icons/Checkmark';
import chevronDown from './icons/ChevronDown';
import chevronLeft from './icons/ChevronLeft';
import chevronRight from './icons/ChevronRight';
import chevronUp from './icons/ChevronUp';
import facebook from './icons/Facebook';
import google from './icons/Google';
import linkedIn from './icons/LinkedIn';
import list from './icons/List';
import location from './icons/Location';
import multipleDates from './icons/MultipleDates';
import pin from './icons/Pin';
import printer from './icons/Printer';
import search from './icons/Search';
import twitter from './icons/Twitter';
import x from './icons/X';
import yahoo from './icons/Yahoo';
import Link from './components/Link';

function CaligramReact() {
  let currentConfig = {
    // Each icon is manually specified to avoid dynamic requires
    icons: {
      checkmark,
      chevronDown,
      chevronLeft,
      chevronRight,
      chevronUp,
      facebook,
      google,
      linkedIn,
      list,
      location,
      multipleDates,
      pin,
      printer,
      search,
      twitter,
      x,
      yahoo,
    },
    linkComponent: Link,
    locale: 'fr',
    locales: {
      fr,
      en,
    },
  };

  function config(newConfig) {
    if (newConfig) {
      currentConfig = deepMerge(currentConfig, newConfig);
    }
    return currentConfig;
  }

  this.config = config;
}

export default new CaligramReact();
