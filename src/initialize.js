import moment from 'moment';

moment.fn.formatTime = function (format, formatWithoutMinutes) {
  if (this.minute() === 0) {
    return this.format(formatWithoutMinutes);
  }
  return this.format(format);
};

moment.updateLocale('fr', {
  weekdaysMin: 'D_L_M_M_J_V_S'.split('_'),
});

moment.updateLocale('en', {
  weekdaysMin: 'S_M_T_W_T_F_S'.split('_'),
});
