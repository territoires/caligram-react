import React, { Component } from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import CaligramReact from '../config';
import t from '../helpers/i18n';
import { getPathFromUrl } from '../helpers/utils';

export default class EventListItem extends Component {
  static propTypes = {
    /** Child DOM elements provided as a custom item template. */
    children: PropTypes.object,
    /** Method used to display the date and time. */
    dateFormat: PropTypes.func,
    /** Event object in the Caligram API format. */
    event: PropTypes.object.isRequired,
    /** Format event to be displayed as part of a linear list, a grid, or a carousel. */
    layout: PropTypes.oneOf(['list', 'grid', 'carousel']),
    /** Date property to use for link generation. */
    linkDate: PropTypes.string,
    /** [Moment](https://momentjs.com/docs/#/i18n/) locale string */
    locale: PropTypes.string,
    /** Placeholder image to be used when the event has no image. */
    placeholderImage: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
    /** Display size of the event. Only used for the grid layout. */
    size: PropTypes.oneOf(['small', 'normal']),
    /** Target attribute to be passed to the link */
    target: PropTypes.string,
  };

  static defaultProps = {
    children: null,
    dateFormat: (event, locale) => {
      const { next_date: nextDate, start_date: startDate } = event;
      return (<span>{moment(nextDate || startDate).format(t(locale, 'time.format'))}</span>);
    },
    layout: 'list',
    linkDate: 'start_date',
    locale: CaligramReact.config().locale,
    placeholderImage: null,
    size: 'normal',
    target: null,
  }

  constructor(props) {
    super();

    moment.locale(props.locale);
  }

  componentWillReceiveProps(nextProps) {
    moment.locale(nextProps.locale);
  }

  render() {
    const itemAttributes = { className: `EventListItem ${this.props.layout} ${this.props.size}` };

    if (this.props.children) {
      return <article {...itemAttributes}>{this.props.children}</article>;
    }

    const { event, placeholderImage } = this.props;
    const Link = CaligramReact.config().linkComponent;

    const date = moment(event[this.props.linkDate]);
    const query = {
      date: date.format('YYYY-MM-DD_HH-mm-ss'),
    };

    if (this.props.layout === 'grid' || this.props.layout === 'carousel') {
      const imageUrl = event.display_image_url || placeholderImage;
      if (imageUrl) {
        itemAttributes.style = { backgroundImage: `url(${imageUrl})` };
      }
    }

    const timeAttributes = {
      children: this.props.dateFormat(event, this.props.locale),
    };

    function getListImage() {
      return placeholderImage || event.display_image_url;
    }

    const eventPath = getPathFromUrl(event.url);

    return (
      <article {...itemAttributes}>
        <Link to={{ pathname: eventPath, query }} target={this.props.target}>
          <div>
            {this.props.layout === 'list' && getListImage() && <img alt={event.title} src={getListImage()} />}
            <time dateTime={date.format('YYYY-MM-DDTHH:mm')} {...timeAttributes} />
            <div className="info">
              <h1>{event.title}</h1>
              {event.venue && <div><p className="venue">{event.venue.name}</p></div>}
            </div>
          </div>
        </Link>
      </article>
    );
  }
}
