import React from 'react';
import PropTypes from 'prop-types';
import Icon from './Icon';

const Checkbox = ({ action, id, title, checked }) => (
  <span className="Checkbox">
    <input type="checkbox" id={id} checked={checked} onChange={action} />
    <label htmlFor={id}>
      <span className={`box ${checked && 'checked'}`}>{checked && <Icon name="checkmark" />}</span>
      {title}
    </label>
  </span>
);

Checkbox.propTypes = {
  /** Action used as the onChange handler on the checkbox. */
  action: PropTypes.func,
  /** Whether the checkbox is checked. */
  checked: PropTypes.bool,
  /** The ID of the checkbox. */
  id: PropTypes.string.isRequired,
  /** The title of the checkbox. */
  title: PropTypes.string.isRequired,
};

Checkbox.defaultProps = {
  action: null,
  checked: false,
};

export default Checkbox;
