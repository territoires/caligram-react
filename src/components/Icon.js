import React from 'react';
import PropTypes from 'prop-types';
import CaligramReact from '../config';

const config = CaligramReact.config();

const Icon = ({ name }) => {
  if (!config.icons[name]) {
    throw new Error(`Icon '${name}' is not specified in caligram-react config`);
  }

  const IconComponent = config.icons[name];
  return <IconComponent className={name} />;
};

Icon.propTypes = {
  /** File name of the icon. */
  name: PropTypes.string.isRequired,
};

export default Icon;
