/* eslint react/prefer-stateless-function: 0 */
/* Cannot be stateless because of Geolocation */

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { geolocated } from 'react-geolocated';
import DynamicMap from './DynamicMap';

class GeolocatedDynamicMap extends Component {
  static propTypes = {
    /** A coords object provided by the Geolocation API
     * Provided by a wrapper component by default, no need to specify. */
    coords: PropTypes.object.isRequired,
    /** A boolean that indicated if the browser supports and has enabled Geolocation.
     * Provided by a wrapper component by default, no need to specify. */
    isGeolocationEnabled: PropTypes.bool,
    /** The properties to pass-through to the DynamicMap */
    mapProps: PropTypes.object.isRequired,
  };

  static defaultProps = {
    isGeolocationEnabled: false,
  };

  render() {
    const mapProperties = Object.assign({}, this.props.mapProps);

    if (this.props.isGeolocationEnabled && this.props.coords) {
      const { coords: { latitude, longitude } } = this.props;
      if (latitude) {
        mapProperties.geoLatitude = latitude;
      }
      if (longitude) {
        mapProperties.geoLongitude = longitude;
      }
    }

    return (
      <DynamicMap {...mapProperties} />
    );
  }
}

export default geolocated({
  userDecisionTimeout: 5000,
})(GeolocatedDynamicMap);

const MockGeolocatedDynamicMap = geolocated({
  userDecisionTimeout: 5000,
  geolocationProvider: {
    getCurrentPosition(onSuccess) {
      return onSuccess({
        coords: {
          latitude: 50,
          longitude: 20,
        },
      });
    },
    watchPosition(onSuccess) {
      return onSuccess({
        coords: {
          latitude: 50,
          longitude: 20,
        },
      });
    },
  },
})(GeolocatedDynamicMap);
export { MockGeolocatedDynamicMap };
