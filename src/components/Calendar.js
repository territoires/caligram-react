import ReactCalendarPane from 'react-calendar-pane';
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import CaligramReact from '../config';
import Icon from './Icon';

export default class Calendar extends Component {
  static propTypes = {
    /** Moment object representing the selected date. */
    date: PropTypes.instanceOf(moment),
    /** Function that is called when a date is selected. */
    onSelect: PropTypes.func.isRequired,
    /** [Moment](https://momentjs.com/docs/#/i18n/) locale string */
    locale: PropTypes.string,
  };

  static defaultProps = {
    date: null,
    locale: CaligramReact.config().locale,
  };

  constructor(props) {
    super();
    moment.locale(props.locale);
    this.ref = this.ref.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    moment.locale(nextProps.locale);
    this.calendar.setState({ date: moment(nextProps.date) });
  }

  previous() {
    this.calendar.previous();
  }

  next() {
    this.calendar.next();
  }

  ref(element) {
    this.calendar = element;
  }

  render() {
    const { locale, date, onSelect } = this.props;

    return (
      <div className="CalendarContainer">
        <button className="previous" onClick={this.previous.bind(this)} type="button">
          <Icon name="chevronLeft" />
        </button>
        <button className="next" onClick={this.next.bind(this)} type="button">
          <Icon name="chevronRight" />
        </button>
        <ReactCalendarPane
          ref={this.ref}
          locale={locale}
          date={date}
          onSelect={onSelect}
          useNav={false}
        />
      </div>
    );
  }
}
