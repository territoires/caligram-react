import React from 'react';
import PropTypes from 'prop-types';

const EventMeta = ({ component, event, siteName, twitterUsername }) => {
  const descriptionPlainText = event.description.replace(/<[^>]*>/g, ' ').replace(/[\s\r]+/g, ' ').trim();
  const meta = {
    title: event.title,
    description: descriptionPlainText,
    meta: {
      charSet: 'utf-8',
      property: {
        'og:site_name': siteName,
        'og:image': event.photo_url,
        'og:locale': 'fr_CA',
        'og:title': event.title,
        'og:description': descriptionPlainText,
        'twitter:card': 'summary',
        'twitter:site': twitterUsername,
        'twitter:creator': twitterUsername,
        'twitter:title': event.title,
        'twitter:description': descriptionPlainText,
        'twitter:image': event.photo_url,
        'twitter:image:width': '200',
        'twitter:image:height': '200',
      },
    },
  };

  const Component = component;
  return (
    <div>
      <Component {...meta} />
    </div>
  );
};

EventMeta.propTypes = {
  /** react-document-meta instance used in the caller instance. */
  component: PropTypes.func.isRequired,
  /** Event object in the Caligram API format. */
  event: PropTypes.object.isRequired,
  /** The name of the website for `og:site_name` */
  siteName: PropTypes.string.isRequired,
  /** The Twitter username to be used for `twitter:site` and `twitter:creator` */
  twitterUsername: PropTypes.string,
};

EventMeta.defaultProps = {
  twitterUsername: '',
};

export default EventMeta;
