import React from 'react';
import PropTypes from 'prop-types';
import CaligramReact from '../config';
import t from '../helpers/i18n';

const EventVenue = ({ locale, room, venue, linkOnVenueName, link }) => {
  // Assemble address lines
  const lines = [];
  if (venue.address) lines.push(venue.address);
  if (venue.address2) lines.push(venue.address2);
  if (venue.city || venue.state || venue.zip) {
    const line = [];
    if (venue.city) line.push(venue.city);
    if (venue.state) line.push(`(${venue.state})`);
    if (venue.zip) line.push(venue.zip);
    lines.push(line.join(' '));
  }

  // Add <br/> elements between lines
  const elements = [];
  for (let i = 0; i < lines.length; i += 1) {
    elements.push(lines[i]);
    if (i < lines.length - 1) {
      elements.push(<br key={i} />);
    }
  }

  const latlong = `${venue.latitude},${venue.longitude}`;
  const directionsURL = link || `http://www.google.com/maps/place/${latlong}/@${latlong},15z`;

  return (
    <div className="EventVenue">
      { venue.name && (
        <div className="name">
          { linkOnVenueName
            ? <a target="_blank" rel="noopener noreferrer" href={directionsURL}>{venue.name}</a>
            : venue.name
          }
        </div>
      ) }
      { room && room.name
        && <div className="room-name">{ room.name }</div>
      }
      { elements.length > 0
        && <address>{ elements }</address>
      }
      { !!venue.latitude && !!venue.longitude && !linkOnVenueName
        && (
        <div className="directions">
          <a target="_blank" rel="noopener noreferrer" href={directionsURL}>{t(locale, 'directions')}</a>
        </div>
        )
      }
    </div>
  );
};

EventVenue.propTypes = {
  /** Custom directions link */
  link: PropTypes.string,
  /** The locale */
  locale: PropTypes.string,
  /** A room as returned by the Caligram API. */
  room: PropTypes.object,
  /** A venue as returned by the Caligram API. */
  venue: PropTypes.object.isRequired,
  linkOnVenueName: PropTypes.bool,
};

EventVenue.defaultProps = {
  link: null,
  locale: CaligramReact.config().locale,
  room: null,
  linkOnVenueName: false,
};

export default EventVenue;
