import React from 'react';
import PropTypes from 'prop-types';
import { hasContact, hasPrice } from '../helpers/event';
import t from '../helpers/i18n';
import Button from './Button';
import EventContact from './EventContact';
import EventDate from './EventDate';
import EventDescription from './EventDescription';
import EventImage from './EventImage';
import EventPrice from './EventPrice';
import EventVenue from './EventVenue';
import EventVideo from './EventVideo';
import HeadingBlock from './HeadingBlock';
import Page from './Page';
import SelectionList from './SelectionList';
import ShareLinks from './ShareLinks';
import StaticMap from './StaticMap';

const EventDetailClassicLayout = (props) => {
  const {
    audienceAction,
    backButtonAction,
    contact,
    event,
    groupAction,
    locale,
    map,
    price,
    selectedDate,
    tagAction,
    typeAction,
  } = props;

  const subtitle = <SelectionList available={event.types} labelKey="name" layout="commaSeparated" appearance="plain" action={typeAction} />;

  return (
    <Page
      className="EventDetail classic"
      layout="custom"
      locale={locale}
      title={event.title}
      subtitle={subtitle}
      backButtonAction={backButtonAction}
    >
      <div className="basicsWrapper">
        <section className="basics">
          <EventDate
            dates={event.dates}
            locale={locale}
            selectedDate={selectedDate}
            url={event.url}
            notes={event.date_notes}
          />
          <EventVenue venue={event.venue} room={event.room} />
          <StaticMap {...map} />
        </section>
      </div>
      <section className="details">
        <div className="description">
          <EventImage src={event.photo_url} />
          <EventDescription description={event.description} />
          <div className="extras">
            { event.tags.length > 0
              && (
              <HeadingBlock title={t(locale, 'tags')} className="tags" inline>
                <SelectionList available={event.tags} labelKey="name" layout="inline" appearance="tag" action={tagAction} />
              </HeadingBlock>
              )
            }
            <HeadingBlock title={t(locale, 'share')} className="share" inline>
              <ShareLinks event={event} icons={['twitter', 'facebook']} />
            </HeadingBlock>
            <HeadingBlock title={t(locale, 'save')} className="save" inline>
              <ShareLinks event={event} icons={['google', 'ical', 'print']} />
            </HeadingBlock>
          </div>
          { event.video && <EventVideo html={event.video.html} /> }
        </div>
        <aside>
          <EventImage src={event.photo_url} />
          { event.facebook_url
            && <Button href={event.facebook_url} title={t(locale, 'facebookEvent')} width="full" newWindow />
          }
          { event.ticket_url
            && <Button href={event.ticket_url} title={t(locale, 'tickets')} type="primary" width="full" newWindow />
          }
          { hasPrice(event)
            && (
            <HeadingBlock title={t(locale, 'price')} className="price">
              <EventPrice locale={locale} {...price} />
            </HeadingBlock>
            )
          }
          { hasContact(event)
            && (
            <HeadingBlock title={t(locale, 'contact')} className="contact">
              <EventContact {...contact} />
            </HeadingBlock>
            )
          }
          { event.audiences.length > 0
            && (
            <HeadingBlock title={t(locale, 'audiences')} className="audiences">
              <SelectionList available={event.audiences} labelKey="name" appearance="plain" action={audienceAction} />
            </HeadingBlock>
            )
          }
          { event.groups.length > 0
            && (
            <HeadingBlock title={t(locale, 'groups')} className="groups">
              <SelectionList available={event.groups} labelKey="name" appearance="plain" action={groupAction} />
            </HeadingBlock>
            )
          }
        </aside>
      </section>
    </Page>
  );
};

EventDetailClassicLayout.propTypes = {
  audienceAction: PropTypes.func.isRequired,
  backButtonAction: PropTypes.func.isRequired,
  contact: PropTypes.object.isRequired,
  event: PropTypes.object.isRequired,
  groupAction: PropTypes.func.isRequired,
  locale: PropTypes.string.isRequired,
  map: PropTypes.object.isRequired,
  price: PropTypes.object.isRequired,
  selectedDate: PropTypes.object.isRequired,
  tagAction: PropTypes.func.isRequired,
  typeAction: PropTypes.func.isRequired,
};

export default EventDetailClassicLayout;
