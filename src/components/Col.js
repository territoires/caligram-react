import React from 'react';
import PropTypes from 'prop-types';

const Col = ({ children, span, total }) => {
  return <div className={`Col span-${span} of-${total}`}>{ children }</div>;
};

Col.propTypes = {
  children: PropTypes.node.isRequired,
  span: PropTypes.number.isRequired,
  total: PropTypes.number,
};

Col.defaultProps = {
  total: 12,
};

export default Col;
