import React, { Component } from 'react';
import PropTypes from 'prop-types';
import MapMarker from './MapMarker';
import CaligramReact from '../config';
import t from '../helpers/i18n';

export default class StaticMap extends Component {
  static propTypes = {
    /** The locale * */
    locale: PropTypes.string,
    /** The Google Static Maps API key. */
    apiKey: PropTypes.string.isRequired,
    /** The height of the map. */
    height: PropTypes.number,
    /** The latitude of the map marker. */
    latitude: PropTypes.string.isRequired,
    /** The longitude of the map marker. */
    longitude: PropTypes.string.isRequired,
    /** The scale of the map. */
    scale: PropTypes.number,
    /** The width of the map. */
    width: PropTypes.number,
    /** The zoom level of the map. */
    zoom: PropTypes.number,
  };

  static defaultProps = {
    height: 170,
    locale: CaligramReact.config().locale,
    scale: 2,
    width: 350,
    zoom: 14,
  };

  render() {
    if (!this.props.longitude && !this.props.latitude) {
      return <div className="StaticMap" />;
    }

    const params = {
      center: `${this.props.latitude},${this.props.longitude}`,
      key: this.props.apiKey,
      zoom: this.props.zoom,
      scale: this.props.scale,
      size: `${this.props.width}x${this.props.height}`,
      maptype: 'roadmap',
    };
    const paramsArray = [];
    Object.keys(params).map((param) => {
      return paramsArray.push(`${encodeURIComponent(param)}=${encodeURIComponent(params[param])}`);
    });
    const paramsString = paramsArray.join('&');
    const latlong = `${this.props.latitude},${this.props.longitude}`;
    const directionsURL = `http://www.google.com/maps/place/${latlong}/@${latlong},15z`;
    const style = { paddingBottom: `${(this.props.height / this.props.width) * 100}%` };

    return (
      <div className="StaticMap">
        <a target="_blank" rel="noopener noreferrer" href={directionsURL} style={style}>
          <img src={`https://maps.googleapis.com/maps/api/staticmap?${paramsString}`} alt={t(this.props.locale, 'venueMap')} />
          <MapMarker />
        </a>
      </div>
    );
  }
}
