import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import { hasContact, hasPrice } from '../helpers/event';
import t from '../helpers/i18n';
import Button from './Button';
import EventContact from './EventContact';
import EventDate from './EventDate';
import EventDescription from './EventDescription';
import EventPrice from './EventPrice';
import EventVenue from './EventVenue';
import EventVideo from './EventVideo';
import HeadingBlock from './HeadingBlock';
import Page from './Page';
import SelectionList from './SelectionList';
import ShareLinks from './ShareLinks';
import StaticMap from './StaticMap';

const EventDetailModernLayout = (props) => {
  const {
    audienceAction,
    backButtonAction,
    contact,
    event,
    groupAction,
    locale,
    map,
    price,
    selectedDate,
    tagAction,
    typeAction,
  } = props;

  const subtitle = <SelectionList available={event.types} labelKey="name" layout="commaSeparated" appearance="plain" action={typeAction} />;

  return (
    <Page
      className="EventDetail modern"
      layout="custom"
      locale={locale}
      title={event.title}
      subtitle={subtitle}
      backButtonAction={backButtonAction}
    >
      <section className="basics">
        <div className="image" style={{ backgroundImage: `url(${event.photo_url})` }}>
          <time dateTime={moment(selectedDate.start_date).format('YYYY-MM-DDTHH:mm')}>
            <span className="day">{ moment(selectedDate.start_date).format('D') }</span>
            <span className="month">{ moment(selectedDate.start_date).format('MMM') }</span>
          </time>
        </div>
        <div className="tickets">
          { event.ticket_url
            && <Button href={event.ticket_url} title={t(locale, 'tickets')} type="primary" width="full" newWindow />
          }
          { hasPrice(event) && <EventPrice locale={locale} {...price} /> }
        </div>
        <EventDate
          dates={event.dates}
          locale={locale}
          selectedDate={selectedDate}
          url={event.url}
          notes={event.date_notes}
        />
        <EventVenue venue={event.venue} room={event.room} />
        <StaticMap {...map} width={367} height={412} />
      </section>
      <div className="detailsWrapper">
        <section className="details">
          <div className="description">
            <EventDescription description={event.description} />
            { event.video && <EventVideo html={event.video.html} /> }
            <div className="extras">
              { event.tags.length > 0
                && (
                  <SelectionList available={event.tags} labelKey="name" layout="inline" appearance="tag" action={tagAction} />
                )
              }
              <ShareLinks event={event} icons={['twitter', 'facebook', 'ical']} />
            </div>
          </div>
          <aside>
            { hasContact(event)
              && (
              <HeadingBlock title={t(locale, 'contact')} className="contact">
                <EventContact {...contact} />
              </HeadingBlock>
              )
            }
            { event.audiences.length > 0
              && (
              <HeadingBlock title={t(locale, 'audiences')} className="audiences">
                <SelectionList available={event.audiences} labelKey="name" appearance="plain" action={audienceAction} />
              </HeadingBlock>
              )
            }
            { event.groups.length > 0
              && (
              <HeadingBlock title={t(locale, 'groups')} className="groups">
                <SelectionList available={event.groups} labelKey="name" appearance="plain" action={groupAction} />
              </HeadingBlock>
              )
            }
          </aside>
        </section>
      </div>
    </Page>
  );
};

EventDetailModernLayout.propTypes = {
  audienceAction: PropTypes.func.isRequired,
  backButtonAction: PropTypes.func.isRequired,
  contact: PropTypes.object.isRequired,
  event: PropTypes.object.isRequired,
  groupAction: PropTypes.func.isRequired,
  locale: PropTypes.string.isRequired,
  map: PropTypes.object.isRequired,
  price: PropTypes.object.isRequired,
  selectedDate: PropTypes.object.isRequired,
  tagAction: PropTypes.func.isRequired,
  typeAction: PropTypes.func.isRequired,
};

export default EventDetailModernLayout;
