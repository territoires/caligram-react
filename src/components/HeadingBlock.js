import React from 'react';
import PropTypes from 'prop-types';
import { classNames } from '../helpers/utils';
import Icon from './Icon';

const HeadingBlock = ({ title, className, icon, children, inline }) => {
  return (
    <section className={classNames(['HeadingBlock', className, { inline }])}>
      <h1>{icon && <Icon name={icon} />}{title}</h1>
      {children}
    </section>
  );
};

HeadingBlock.propTypes = {
  /** The title of the block. */
  title: PropTypes.string.isRequired,
  /** The CSS class of the block. */
  className: PropTypes.string,
  /** File name of the icon. */
  icon: PropTypes.string,
  /** The content of the block. */
  children: PropTypes.any,
  /** Whether the title should be displayed inline alongside the content */
  inline: PropTypes.bool,
};

HeadingBlock.defaultProps = {
  className: null,
  icon: null,
  children: null,
  inline: false,
};

export default HeadingBlock;
