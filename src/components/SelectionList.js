import React, { Component } from 'react';
import PropTypes from 'prop-types';
import SelectableLabel from './SelectableLabel';

export default class SelectionList extends Component {
  static propTypes = {
    /** Function with an item as its argument that is called on a click. */
    action: PropTypes.func,
    /** The appearance of SelectableLabels. */
    appearance: PropTypes.oneOf(['plain', 'filter', 'tag']),
    /** All available items. */
    available: PropTypes.array.isRequired,
    /** Function to clear the selection */
    clear: PropTypes.func,
    /** Whether to display the clear button first or last */
    clearPosition: PropTypes.oneOf(['first', 'last']),
    /** Text to display on the "Clear" or "Show all" item, the first of the list. */
    clearText: PropTypes.string,
    /** Number of grid columns when using the `grid` layout. */
    columns: PropTypes.number,
    /** Key in each item of available to use as as className. */
    customClassKey: PropTypes.string,
    /** Class name to apply to the SelectionList element. */
    customClassName: PropTypes.string,
    /** Key in each item of available to use as a group */
    groupKey: PropTypes.string,
    /** Whether to show the Clear button when there are no selected items. */
    hideClearWhenEmpty: PropTypes.bool,
    /** The name of the icon to pass to SelectionLabel for selected items. */
    iconSelected: PropTypes.string,
    /** The name of the icon to pass to SelectionLabel for unselected items. */
    iconUnselected: PropTypes.string,
    /** Key in each item of available to use as a key (unique identifier). Fallbacks on index. */
    idKey: PropTypes.string,
    /** Key in each item of available to use as a label */
    labelKey: PropTypes.string,
    /** Display labels as a linear list, an inline list, or a grid. */
    layout: PropTypes.oneOf(['list', 'grid', 'inline', 'commaSeparated']),
    /** Selected items. */
    selected: PropTypes.array,
  };

  static defaultProps = {
    action: () => {},
    appearance: 'filter',
    clear: () => {},
    clearPosition: 'first',
    clearText: '',
    columns: 2,
    customClassName: '',
    customClassKey: 'class',
    groupKey: 'group',
    hideClearWhenEmpty: false,
    iconSelected: 'x',
    iconUnselected: null,
    idKey: 'id',
    labelKey: 'label',
    selected: [],
    layout: 'list',
  };

  constructor(props) {
    super(props);

    this.state = {
      available: this.props.available,
      selected: this.props.selected,
      hasSelected: this.props.selected.length > 0,
    };

    this.isSelected = this.isSelected.bind(this);
    this.hasSelected = this.hasSelected.bind(this);
    this.handleClick = this.handleClick.bind(this);
    this.handleClear = this.handleClear.bind(this);
    this.defaultAction = this.defaultAction.bind(this);
  }

  componentWillReceiveProps(newProps) {
    const { available, selected } = newProps;

    this.setState(prevState => ({
      ...prevState,
      available,
      selected,
    }));
  }

  isSelected(item) {
    return (
      this.state.selected.indexOf(item) > -1
      || this.state.selected.find((sel) => {
        return sel.id && sel.id === item.id
          && (
            !sel[this.props.groupKey]
            || sel[this.props.groupKey] === item[this.props.groupKey]
          );
      }) !== undefined
    );
  }

  hasSelected() {
    return this.state.selected.length > 0;
  }

  handleClick(item) {
    return this.props.action.call(this, item);
  }

  handleClear() {
    return this.props.clear.call(this);
  }

  handleKeyPress(item, event) {
    if (event.key === 'Enter') {
      this.handleClick(item);
    }
  }

  defaultAction(item) {
    return SelectionList.defaultAction.call(this, item);
  }

  render() {
    const filters = this.state.available.map((item, i) => {
      const groupSlug = item[this.props.groupKey] && item[this.props.groupKey].replace(/\W+/g, '-').toLowerCase();

      return (
        <li
          className={item[this.props.customClassKey]}
          key={`${groupSlug ? `${groupSlug}-` : ''}${item[this.props.idKey]}` || i}
        >
          <SelectableLabel
            action={this.handleClick.bind(this, item)}
            label={item[this.props.labelKey]}
            group={item[this.props.groupKey]}
            selected={this.isSelected(item)}
            appearance={this.props.appearance}
            iconSelected={this.props.iconSelected}
            iconUnselected={this.props.iconUnselected}
          />
        </li>
      );
    });

    if (this.props.clearText && (!this.props.hideClearWhenEmpty || this.hasSelected())) {
      const clear = (
        <li className="clear" key="clear">
          <SelectableLabel
            action={this.handleClear}
            label={this.props.clearText}
            selected={!this.hasSelected()}
            iconSelected={this.props.iconSelected || 'checkmark'}
            iconUnselected={this.props.iconUnselected || 'x'}
          />
        </li>
      );

      if (this.props.clearPosition === 'last') {
        filters.push(clear);
      } else {
        filters.unshift(clear);
      }
    }

    const layoutClassNames = `${this.props.layout === 'grid' ? `columns-${this.props.columns}` : ''} `;
    const listClassNames = `${this.hasSelected() ? 'selected' : ''} ${this.props.layout}`;

    return (
      <div className={`${layoutClassNames}`}>
        <ul className={`SelectionList ${this.props.customClassName} ${listClassNames}`}>
          {filters}
        </ul>
      </div>
    );
  }
}
