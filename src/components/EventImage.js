import React from 'react';
import PropTypes from 'prop-types';

const EventImage = ({ src, alt, credit }) => {
  if (src) {
    return (
      <div className="EventImage">
        <img src={src} alt={alt} />
        { credit && <div className="credit">{ credit }</div>}
      </div>
    );
  }

  return null;
};

EventImage.propTypes = {
  alt: PropTypes.string,
  credit: PropTypes.string,
  src: PropTypes.string,
};

EventImage.defaultProps = {
  alt: '',
  credit: '',
  src: null,
};

export default EventImage;
