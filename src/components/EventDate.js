import React, { Component } from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';

import Icon from './Icon';
import Popover from './Popover';

import CaligramReact from '../config';
import { getPathFromUrl } from '../helpers/utils';
import t from '../helpers/i18n';

export default class EventDate extends Component {
  static propTypes = {
    /** Array of dates as returned by the Caligram API. */
    dates: PropTypes.array.isRequired,
    /** [Moment](https://momentjs.com/docs/#/i18n/) locale string */
    locale: PropTypes.string,
    /** ID of the currently selected date. */
    selectedDate: PropTypes.any,
    /** The absolute path of the event. Used for inner navigation. */
    url: PropTypes.string.isRequired,
    notes: PropTypes.string,
  };

  static defaultProps = {
    locale: CaligramReact.config().locale,
    selectedDate: null,
    notes: null,
  }

  constructor(props) {
    super(props);

    moment.locale(this.props.locale);

    let { selectedDate } = this.props;
    if (!selectedDate) {
      [selectedDate] = this.props.dates;
    }

    this.state = {
      showMoreDatesPopover: false,
      selectedDate,
    };
  }

  componentWillReceiveProps(nextProps) {
    moment.locale(nextProps.locale);
  }

  getMoreDatesList() {
    const { dates, locale, url } = this.props;
    const Link = CaligramReact.config().linkComponent;

    return (
      <ul className="moreDates">
        { dates.map((date) => {
          const formattedDate = moment(date.start_date).format(t(locale, 'date.format'));
          const formattedTime = moment(date.start_date).formatTime(
            t(locale, 'time.format'),
            t(locale, 'time.withoutMinutes'),
          );
          const pathname = getPathFromUrl(url);
          const selected = this.state.selectedDate.id === date.id;
          const props = selected ? { className: 'selected' } : {};
          const query = { date: moment(date.start_date).format('YYYY-MM-DD_HH-mm-ss') };
          return (
            <li key={date.id} {...props}>
              <Link to={{ pathname, query }} onClick={this.handleDateSelection.bind(this, date)}>
                {selected && <Icon name="checkmark" />}
                {`${formattedDate} – ${formattedTime}`}
              </Link>
            </li>
          );
        }) }
      </ul>
    );
  }

  handleDateSelection(selectedDate) {
    return this.setState({
      selectedDate,
      showMoreDatesPopover: false,
    });
  }

  handleKeyPress(event) {
    if (event.key === 'Enter') {
      this.toggleMoreDates();
    }
  }

  toggleMoreDates() {
    return this.setState((prevState) => {
      return { showMoreDatesPopover: !prevState.showMoreDatesPopover };
    });
  }

  render() {
    const {
      dates,
      locale,
      notes,
    } = this.props;
    const { selectedDate } = this.state;
    const momentDate = moment(selectedDate.start_date);
    const formattedDate = momentDate.format(t(locale, 'date.format'));
    const formattedStartTime = momentDate.formatTime(
      t(locale, 'time.format'),
      t(locale, 'time.withoutMinutes'),
    );
    const startDate = moment(dates[0].start_date)
      .format(t(locale, 'date.short'));
    const endDate = moment(dates[dates.length - 1].start_date)
      .format(t(locale, 'date.short'));
    const otherDatesCount = dates.length - 1;
    let formattedTime;

    if (selectedDate.duration > 0) {
      const endTime = momentDate.clone().add(selectedDate.duration, 'seconds');
      const formattedEndTime = endTime.formatTime(
        t(locale, 'time.format'),
        t(locale, 'time.withoutMinutes'),
      );
      formattedTime = `${formattedStartTime} ${t(locale, 'time.rangeDelimiter')} ${formattedEndTime}`;
    } else if (momentDate.format('HHmmss') !== '000000') {
      formattedTime = formattedStartTime;
    }

    return (
      <div className="EventDate">
        <div className="date">{formattedDate}</div>
        { formattedTime && <div className="time">{formattedTime}</div> }
        { notes && <div className="notes" dangerouslySetInnerHTML={{ __html: notes }} /> }
        { dates.length > 1
          && (
          <div className="otherDates">
            <Popover
              visible={this.state.showMoreDatesPopover}
              dropdownView={this.getMoreDatesList()}
              clickOverlay={this.toggleMoreDates.bind(this)}
            >
              <button type="button" className="count" onClick={this.toggleMoreDates.bind(this)} onKeyPress={this.handleKeyPress.bind(this)}>
                <Icon name="multipleDates" />
                <span>{otherDatesCount} {t(locale, 'otherDates', otherDatesCount)}</span>
                <Icon name="chevronDown" />
              </button>
            </Popover>
            <span className="range">{t(locale, 'date.from')} {startDate} {t(locale, 'date.to')} {endDate}</span>
          </div>
          )
        }
      </div>
    );
  }
}
