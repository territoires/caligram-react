import React from 'react';
import PropTypes from 'prop-types';
import CaligramReact from '../config';
import t from '../helpers/i18n';
import Button from './Button';

const Page = ({ locale, title, subtitle, backButtonAction, className, layout, children }) => {
  return (
    <article className={`Page ${className || ''}`}>
      { (title || backButtonAction)
        && (
          <header>
            { backButtonAction && <Button title={t(locale, 'allEvents')} iconLeft="chevronLeft" type="borderless" action={backButtonAction} className="back" /> }
            { title && <h1>{title}</h1> }
            { subtitle && <h2>{subtitle}</h2> }
          </header>
        )
      }
      { layout === 'custom'
        ? children
        : (
          <section className={`content ${layout}`}>
            <div className="body">
              {children}
            </div>
          </section>
        )
      }
    </article>
  );
};

Page.propTypes = {
  /** The locale * */
  locale: PropTypes.string,
  /** The page title. */
  title: PropTypes.string,
  /** The page subtitle. */
  subtitle: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
  /** If present, shows a back button that triggers the provided action. */
  backButtonAction: PropTypes.func,
  /** The CSS class of the article element. */
  className: PropTypes.string,
  /** The layout to use for the content section */
  layout: PropTypes.oneOf(['narrow', 'wide', 'custom']),
  /** The page content. */
  children: PropTypes.any,
};

Page.defaultProps = {
  backButtonAction: null,
  children: null,
  className: null,
  layout: 'narrow',
  locale: CaligramReact.config().locale,
  subtitle: null,
  title: null,
};

export default Page;
