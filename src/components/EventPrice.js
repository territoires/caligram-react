import React, { Component } from 'react';
import PropTypes from 'prop-types';
import t from '../helpers/i18n';
import CaligramReact from '../config';

export default class EventPrice extends Component {
  static propTypes = {
    /** Three-letter currency code. */
    currency: PropTypes.string,
    /** The locale string * */
    locale: PropTypes.string,
    /** Notes about the price. */
    notes: PropTypes.string,
    /** Price type: free, fixed, or PWYC. */
    type: PropTypes.oneOf(['FREE', 'FIXED', 'DONATION']).isRequired,
    /** List of prices. */
    prices: PropTypes.arrayOf(PropTypes.object),
  };

  static defaultProps = {
    currency: null,
    locale: CaligramReact.config().locale,
    notes: null,
    prices: [],
  };

  render() {
    const { currency, locale, notes, type, prices } = this.props;
    const typeLabels = {
      FREE: t(locale, 'priceTypes.free'),
      DONATION: t(locale, 'priceTypes.donation'),
    };
    const sortedPrices = prices.slice().sort((a, b) => {
      return Number.parseFloat(a.value) - Number.parseFloat(b.value);
    });

    if (type === 'FIXED' && prices.length === 0) {
      return null;
    }

    return (
      <div className="EventPrice">
        { typeLabels[type] && <span className="label">{ typeLabels[type] }</span> }
        { type !== 'FREE' && prices.length > 0
          && (
          <ul className="prices">
            { sortedPrices.map(price => (
              <li key={price.id}>
                <strong>{price.value} ${currency}</strong> – {price.description}
              </li>
            )) }
          </ul>
          )
        }
        { notes && <p className="notes">{notes}</p> }
      </div>
    );
  }
}
