import React from 'react';
import PropTypes from 'prop-types';

const EventVideo = ({ html }) => {
  return (
    <div className="EventVideo" dangerouslySetInnerHTML={{ __html: html }} />
  );
};

EventVideo.propTypes = {
  /** The HTML code to display the video. */
  html: PropTypes.string.isRequired,
};

export default EventVideo;
