import React from 'react';
import PropTypes from 'prop-types';
import ResponsiveDropdown from 'react-responsive-dropdown';

const Popover = ({ children, clickOverlay, dropdownView, overlayOptions, visible, width }) => {
  return (
    <ResponsiveDropdown
      arrowSize={10}
      borderColor="#ccc"
      borderRadius={0}
      borderWidth={2}
      clickOverlay={clickOverlay}
      dropdownView={<div className="Popover">{dropdownView}</div>}
      visible={visible}
      width={width}
      {...overlayOptions}
    >
      {children}
    </ResponsiveDropdown>
  );
};

Popover.propTypes = {
  /** Child DOM elements comprising the content of the popover. */
  children: PropTypes.any,
  /** Function to be called when clicking outside the popover. */
  clickOverlay: PropTypes.func,
  /** The content of the popover. */
  dropdownView: PropTypes.element,
  /** Overlay options. */
  overlayOptions: PropTypes.object,
  /** Whether the popover is visible. */
  visible: PropTypes.bool,
  /** Width of the popover. */
  width: PropTypes.number,
};

Popover.defaultProps = {
  children: null,
  clickOverlay: null,
  dropdownView: null,
  overlayOptions: {},
  visible: false,
  width: 300,
};

export default Popover;
