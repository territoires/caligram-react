import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Button from './Button';
import CaligramReact from '../config';
import t from '../helpers/i18n';

export default class Pagination extends Component {
  static propTypes = {
    /** The locale * */
    locale: PropTypes.string,
    /** The action to handle page change. */
    action: PropTypes.func.isRequired,
    /** The current page number. */
    currentPage: PropTypes.number.isRequired,
    /** The total number of pages. */
    totalPages: PropTypes.number.isRequired,
  }

  static defaultProps = {
    locale: CaligramReact.config().locale,
  }

  componentWillReceiveProps() {
    this.previousButton.blur();
    this.nextButton.blur();
  }

  handlePageChange(page) {
    return this.props.action.call(this, page);
  }

  previousRef(button) {
    this.previousButton = button;
  }

  nextRef(button) {
    this.nextButton = button;
  }

  previousRef = this.previousRef.bind(this);

  nextRef = this.nextRef.bind(this);

  render() {
    const { currentPage, locale, totalPages } = this.props;

    const previousPage = currentPage - 1;
    const nextPage = currentPage + 1;

    const previousProps = {
      className: 'previous',
      disabled: currentPage <= 1,
      action: this.handlePageChange.bind(this, previousPage),
      elementRef: this.previousRef.bind(this),
      iconLeft: 'chevronLeft',
      title: t(locale, 'previousPage'),
    };

    const nextProps = {
      className: 'next',
      disabled: currentPage >= totalPages,
      action: this.handlePageChange.bind(this, nextPage),
      elementRef: this.nextRef.bind(this),
      iconRight: 'chevronRight',
      title: t(locale, 'nextPage'),
    };

    return (
      <div className="Pagination">
        <Button {...previousProps} />
        <Button {...nextProps} />
      </div>
    );
  }
}
