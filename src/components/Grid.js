import React from 'react';
import PropTypes from 'prop-types';
import Col from './Col';

function renderChildren(children, cols) {
  return React.Children.map(children, (child) => {
    if (child.type === Col) {
      return React.cloneElement(child, { total: cols });
    }
    return child;
  });
}

const Grid = ({ children, className, cols }) => {
  return <div className={`Grid ${className || ''}`}>{ renderChildren(children, cols) }</div>;
};

Grid.propTypes = {
  children: PropTypes.node.isRequired,
  className: PropTypes.string,
  cols: PropTypes.number,
};

Grid.defaultProps = {
  className: '',
  cols: 12,
};

export default Grid;
