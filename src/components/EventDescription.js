import React from 'react';
import PropTypes from 'prop-types';

const EventDescription = ({ description }) => {
  if (description) {
    return (
      <div className="EventDescription" dangerouslySetInnerHTML={{ __html: description }} />
    );
  }
  return null;
};

EventDescription.propTypes = {
  /** The HTML description. */
  description: PropTypes.string,
};

EventDescription.defaultProps = {
  description: null,
};

export default EventDescription;
