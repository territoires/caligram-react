import React, { Component } from 'react';
import PropTypes from 'prop-types';
import groupBy from 'lodash.groupby';
import moment from 'moment';
import Velocity from 'velocity-react/lib/velocity-animate-shim';

import CaligramReact from '../config';
import ActiveFilters from './ActiveFilters';
import EventListItem from './EventListItem';
import Button from './Button';
import Pagination from './Pagination';
import t from '../helpers/i18n';
import { classNames } from '../helpers/utils';

export default class EventList extends Component {
  static propTypes = {
    /** Content to be appended before ActiveFilters */
    beforeFilters: PropTypes.element,
    /** Content to be appended before the event list */
    beforeEvents: PropTypes.element,
    /** A CSS class to be appended to the default list of classes */
    className: PropTypes.string,
    /** Function to call when all filters are cleared */
    clearFilters: PropTypes.func,
    /** Number of grid columns when using the `grid` layout. */
    columns: PropTypes.number,
    /** Total number of columns in the container when using the `grid` layout. */
    container: PropTypes.number,
    /** Current page number */
    currentPage: PropTypes.number,
    /** Method used to display the date and time. */
    dateFormat: PropTypes.func,
    /** The message to display when there are no events. */
    emptyMessage: PropTypes.string,
    /** Array of event objects in the Caligram API format. */
    events: PropTypes.array,
    /** Currently active filters. */
    filters: PropTypes.array,
    /** Groups events into `&lt;section&gt;`s based on event dates. */
    groupBy: PropTypes.oneOf(['week', 'day', 'none']),
    /** Display events in a linear list, as a grid, or as a scrolling carousel. */
    layout: PropTypes.oneOf(['list', 'grid', 'carousel']),
    /** Date property to use for link generation. */
    linkDate: PropTypes.string,
    /** Whether the events are currently loading */
    loading: PropTypes.bool,
    /** [Moment](https://momentjs.com/docs/#/i18n/) locale string */
    locale: PropTypes.string,
    /** Function to call when changing page. */
    pageChange: PropTypes.func,
    /** Placeholder image to be used when the event has no image. */
    placeholderImage: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
    /** Function to call when a filter is removed */
    removeFilter: PropTypes.func,
    /** Template for children events. A method that takes an event and returns React elements */
    template: PropTypes.func,
    /** Display size of the events. Only used for the grid layout. */
    size: PropTypes.oneOf(['small', 'normal']),
    /** Target attribute to be passed to the link */
    target: PropTypes.string,
    /** Optional title to display above the list */
    title: PropTypes.string,
    /** Total number of pages */
    totalPages: PropTypes.number,
  };

  static defaultProps = {
    beforeEvents: null,
    beforeFilters: null,
    className: null,
    clearFilters: null,
    columns: 3,
    container: 12,
    currentPage: 1,
    dateFormat: undefined,
    emptyMessage: null,
    events: null,
    filters: [],
    groupBy: 'day',
    layout: 'list',
    linkDate: undefined,
    loading: false,
    locale: CaligramReact.config().locale,
    pageChange: null,
    placeholderImage: null,
    removeFilter: null,
    size: 'normal',
    target: '',
    template: null,
    title: null,
    totalPages: null,
  }

  constructor(props) {
    super();
    moment.locale(props.locale);
    this.setEventsRef = this.setEventsRef.bind(this);
    this.scrollRight = this.scroll.bind(this, false);
    this.scrollLeft = this.scroll.bind(this, true);
  }

  state = {
    showPrevButton: false,
    showNextButton: true,
  }

  componentWillReceiveProps(nextProps) {
    moment.locale(nextProps.locale);
  }

  getListItems(events) {
    const transferableProps = {
      dateFormat: this.props.dateFormat,
      layout: this.props.layout,
      linkDate: this.props.linkDate,
      locale: this.props.locale,
      placeholderImage: this.props.placeholderImage,
      size: this.props.size,
      target: this.props.target,
    };

    if (this.props.template) {
      const { template } = this.props;
      return events.map((event) => {
        return (
          <EventListItem key={`${event.id}-${moment(event.next_date || event.start_date).format('YYYYMMDD')}`} event={event} {...transferableProps}>
            {template(event)}
          </EventListItem>
        );
      });
    }

    return events.map((event) => {
      return <EventListItem key={`${event.id}-${moment(event.next_date || event.start_date).format('YYYYMMDD')}`} event={event} {...transferableProps} />;
    });
  }

  eventList() {
    const { currentPage, events, locale, pageChange, totalPages } = this.props;
    const groupByFormat = this.props.groupBy === 'week' ? 'ggggww' : 'YYYYMMDD';
    let eventList;

    if (this.props.groupBy === 'none') {
      eventList = this.getListItems(events);
    } else {
      const sections = groupBy(events, (event) => {
        return moment(event.next_date || event.start_date).format(groupByFormat);
      });

      eventList = Object.keys(sections).map((section) => {
        const sectionDay = moment(section, groupByFormat);
        return (
          <section key={section}>
            <header>
              <time dateTime={sectionDay.format('YYYY-MM-DD')}>
                {this.props.groupBy === 'week' && `${t(locale, 'weekOf')} `}{sectionDay.format(t(locale, 'date.short'))}
              </time>
            </header>
            <div>
              {this.getListItems(sections[section])}
            </div>
          </section>
        );
      });
    }

    if (currentPage && totalPages > 1) {
      eventList.push(
        <Pagination key="pagination" locale={locale} action={pageChange} currentPage={currentPage} totalPages={totalPages} />,
      );
    }

    return eventList;
  }

  setEventsRef(element) {
    this.eventsRef = element;
    if (this.eventsRef) {
      this.eventsRef.addEventListener('scroll', (evt) => {
        const scrollMax = evt.target.scrollWidth - evt.target.offsetWidth;
        this.setState({
          showPrevButton: evt.target.scrollLeft > 0,
          showNextButton: evt.target.scrollLeft < scrollMax,
        });
      });
    }
  }

  scroll(left) {
    const containerWidth = this.eventsRef.offsetWidth;
    let scrollToIndex = left ? 0 : this.eventsRef.childNodes.length - 1;
    for (let i = 0; i < this.eventsRef.childNodes.length; i += 1) {
      const el = this.eventsRef.childNodes[i];
      const wiggleRoom = el.offsetWidth * 0.1;
      const relativeX = el.offsetLeft - this.eventsRef.offsetLeft;
      const isOffscreenRight = relativeX - this.eventsRef.scrollLeft > containerWidth - wiggleRoom;
      const isOffscreenLeft = this.eventsRef.scrollLeft - relativeX < containerWidth + wiggleRoom;
      if ((!left && isOffscreenRight) || (left && isOffscreenLeft)) {
        scrollToIndex = i;
        break;
      }
    }
    Velocity(this.eventsRef.childNodes[scrollToIndex], 'scroll', {
      axis: 'x',
      duration: 500,
      container: this.eventsRef,
    }, [250, 15]);
  }

  render() {
    const {
      beforeFilters,
      beforeEvents,
      className,
      clearFilters,
      emptyMessage,
      events,
      filters,
      locale,
      removeFilter,
      title,
    } = this.props;

    const classes = classNames('EventList', className, {
      [`columns-${this.props.columns} container-${this.props.container}`]: this.props.layout === 'grid' || this.props.layout === 'carousel',
      loading: this.props.loading,
      carousel: this.props.layout === 'carousel',
    });
    const hasEvents = events && events.length > 0;

    return (
      <div className={classes}>
        { title && <h2>{title}</h2> }
        { beforeFilters }
        <ActiveFilters
          locale={locale}
          key="filters"
          selected={filters}
          action={removeFilter}
          clear={clearFilters}
        />
        { beforeEvents }
        { hasEvents ? (
          <div className="events" ref={this.setEventsRef}>{this.eventList()}</div>
        ) : (
          <p className="empty">{emptyMessage}</p>
        )}
        { hasEvents && this.props.layout === 'carousel' && (
          <nav>
            { this.state.showPrevButton && (
              <Button className="prev" action={this.scrollLeft} title="Défiler le carousel vers la gauche" iconLeft="chevronLeft" hideTitle />
            )}
            { this.state.showNextButton && (
              <Button className="next" action={this.scrollRight} title="Défiler le carousel vers la droite" iconLeft="chevronRight" hideTitle />
            )}
          </nav>
        )}
      </div>
    );
  }
}
