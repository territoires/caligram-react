/* global window */
import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import Icon from './Icon';

// In part adapted from https://github.com/jasonsalzman/react-add-to-calendar
function formatTime(date) {
  const formattedDate = moment.utc(date).format('YYYYMMDDTHHmmssZ');
  return formattedDate.replace('+00:00', 'Z');
}

function calculateDuration(startTime, endTime) {
  // snag parameters and format properly in UTC
  const end = moment.utc(endTime).format('DD/MM/YYYY HH:mm:ss');
  const start = moment.utc(startTime).format('DD/MM/YYYY HH:mm:ss');

  // calculate the difference in milliseconds between the start and end times
  const difference = moment(end, 'DD/MM/YYYY HH:mm:ss').diff(
    moment(start, 'DD/MM/YYYY HH:mm:ss'),
  );

  // convert difference from above to a proper momentJs duration object
  const duration = moment.duration(difference);

  return (
    Math.floor(duration.asHours()) + moment.utc(difference).format(':mm')
  );
}

function buildIcalLocation(venue) {
  return `${venue.name} (${venue.address}, ${venue.city} ${venue.zip})`;
}

function buildShareUrl(event, type) {
  const match = event.url.match(/(?:https?:\/\/|)([^/]+).*/);
  const domain = match[1];
  const duration = calculateDuration(event.start_date, event.end_date);

  let calendarUrl = '';

  switch (type) {
    case 'twitter':
      calendarUrl = `https://twitter.com/home?status=${event.url}`;
      break;

    case 'facebook':
      calendarUrl = `https://www.facebook.com/sharer/sharer.php?u=${event.url}`;
      break;

    case 'linkedIn':
      calendarUrl = `https://www.linkedin.com/shareArticle?mini=true&url=${event.url}&title=${encodeURIComponent(event.title)}`;
      break;

    case 'google':
      calendarUrl = 'https://calendar.google.com/calendar/render';
      calendarUrl += '?action=TEMPLATE';
      calendarUrl += `&dates=${formatTime(event.start_date)}`;
      calendarUrl += `/${formatTime(event.end_date)}`;
      calendarUrl += `&location=${encodeURIComponent(buildIcalLocation(event.venue))}`;
      calendarUrl += `&text=${encodeURIComponent(event.title)}`;
      calendarUrl += `&details=${encodeURIComponent(event.description)}`;
      break;

    case 'outlookcom':
      calendarUrl = 'https://outlook.live.com/owa/?rru=addevent';
      calendarUrl += `&startdt=${formatTime(event.start_date)}`;
      calendarUrl += `&enddt=${formatTime(event.end_date)}`;
      calendarUrl += `&subject=${encodeURIComponent(event.title)}`;
      calendarUrl += `&location=${encodeURIComponent(buildIcalLocation(event.venue))}`;
      calendarUrl += `&body=${encodeURIComponent(event.description)}`;
      calendarUrl += '&allday=false';
      calendarUrl += `&uid=${event.id}@${domain}`;
      calendarUrl += '&path=/calendar/view/Month';
      break;

    case 'yahoo':
      calendarUrl = 'https://calendar.yahoo.com/?v=60&view=d&type=20';
      calendarUrl += `&title=${encodeURIComponent(event.title)}`;
      calendarUrl += `&st=${formatTime(event.start_date)}`;
      calendarUrl += `&dur=${duration}`;
      calendarUrl += `&desc=${encodeURIComponent(event.description)}`;
      calendarUrl += `&in_loc=${encodeURIComponent(buildIcalLocation(event.venue))}`;
      break;

    default:
      calendarUrl = `/api/events/${event.id}.ics`;
  }

  return calendarUrl;
}

function handlePrint(ev) {
  window.print();
  ev.preventDefault();
  return false;
}

function handleKeyPress(event) {
  if (event.key === 'Enter') {
    handlePrint();
  }
}

const ShareLinks = ({ event, icons, noTint }) => {
  if (event.url === undefined) {
    return null;
  }

  return (
    <div className={`ShareLinks ${noTint && 'noTint'}`}>
      <ul>
        {icons.map((icon) => {
          switch (icon) {
            case 'ical':
              return (
                <li key={icon}><a target="_blank" rel="noopener noreferrer" href={buildShareUrl(event, 'ical')}>iCal</a></li>
              );
            case 'print':
              return (
                <li key={icon}><span onClick={handlePrint} onKeyPress={handleKeyPress}><Icon name="printer" /></span></li>
              );
            default:
              return (
                <li key={icon}><a href={buildShareUrl(event, icon)} target="_blank" rel="noopener noreferrer"><Icon name={icon} /></a></li>
              );
          }
        })}
      </ul>
    </div>
  );
};

ShareLinks.propTypes = {
  /** The event to export. */
  event: PropTypes.object.isRequired,
  /** A list of icons to display. twitter, facebook, linkedIn, google, yahoo, ical, print */
  icons: PropTypes.arrayOf(PropTypes.string),
  /** Whether icons should be tinted with the primary color */
  noTint: PropTypes.bool,
};

ShareLinks.defaultProps = {
  icons: ['twitter', 'facebook', 'google', 'yahoo', 'ical', 'print'],
  noTint: false,
};

export default ShareLinks;
