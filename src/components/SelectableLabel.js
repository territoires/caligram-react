import React from 'react';
import PropTypes from 'prop-types';
import Icon from './Icon';
import { handleKeyPress } from '../helpers/utils';

const SelectableLabel = (props) => {
  const { action, appearance, label, group, selected, iconUnselected, iconSelected } = props;
  const selectedClass = selected ? 'selected' : '';
  const icon = (selected && iconSelected) || (!selected && iconUnselected);

  return (
    <button type="button" className={`SelectableLabel ${appearance} ${selectedClass}`} onClick={action} onKeyPress={handleKeyPress(action)}>
      {group && <span className="group">{group}</span>}
      {label}
      {icon && <Icon name={icon} />}
    </button>
  );
};

SelectableLabel.propTypes = {
  /** Action triggered when the label is clicked. */
  action: PropTypes.func,
  /** Which style to apply. */
  appearance: PropTypes.oneOf(['plain', 'filter', 'tag']),
  /** Label for the selectable item. */
  label: PropTypes.string.isRequired,
  /** Group (displayed to the left of the label) for the selectable item. */
  group: PropTypes.string,
  /** Selected status. A boolean. */
  selected: PropTypes.bool,
  /** The name of the icon to append to the label when it is selected. */
  iconSelected: PropTypes.string,
  /** The name of the icon to append to the label when it is not selected. */
  iconUnselected: PropTypes.string,
};

SelectableLabel.defaultProps = {
  action: null,
  appearance: 'filter',
  group: null,
  iconSelected: 'x',
  iconUnselected: null,
  selected: false,
};

export default SelectableLabel;
