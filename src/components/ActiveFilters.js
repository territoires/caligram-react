import React from 'react';
import PropTypes from 'prop-types';
import SelectionList from './SelectionList';
import CaligramReact from '../config';
import t from '../helpers/i18n';

const ActiveFilters = ({ locale, selected, action, clear, groupKey, labelKey }) => {
  if (selected.length > 0) {
    return (
      <SelectionList
        action={action}
        available={selected}
        clear={clear}
        clearPosition="last"
        clearText={t(locale, 'clearFilters')}
        customClassName="ActiveFilters"
        groupKey={groupKey}
        hideClearWhenEmpty
        labelKey={labelKey}
        layout="inline"
        selected={selected}
      />
    );
  }

  return null;
};

ActiveFilters.propTypes = {
  /** The locale * */
  locale: PropTypes.string,
  /** Function with an item as its argument that is called on a click. */
  action: PropTypes.func,
  /** Function to clear the selection */
  clear: PropTypes.func,
  /** Key in each item of available to use as a label */
  labelKey: PropTypes.string,
  /** Key in each item of available to use as a group */
  groupKey: PropTypes.string,
  /** Active filter items. */
  selected: PropTypes.array,
};

ActiveFilters.defaultProps = {
  action: null,
  clear: null,
  selected: [],
  labelKey: 'label',
  locale: CaligramReact.config().locale,
  groupKey: 'tag',
};

export default ActiveFilters;
