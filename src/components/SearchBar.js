import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Autosuggest from 'react-autosuggest';
import Icon from './Icon';
import CaligramReact from '../config';
import t from '../helpers/i18n';

export default class SearchBar extends Component {
  static propTypes = {
    /** Text to display on the submit button (adaptive behaviour, not actually displayed) */
    buttonText: PropTypes.string,
    /** Object key to use to get the suggestion label */
    labelKey: PropTypes.string,
    /** The locale string * */
    locale: PropTypes.string,
    /** Minimal length before the search endpoint is queried */
    minLength: PropTypes.number,
    /** Mandatory method to update the input value */
    onChange: PropTypes.func.isRequired,
    /** Mandatory method called when the suggestions should be cleared */
    onClear: PropTypes.func.isRequired,
    /** Mandatory method called when the suggestions should be fetched */
    onFetch: PropTypes.func.isRequired,
    /** Mandatory method called when a suggestion is selected */
    onSelect: PropTypes.func.isRequired,
    /** Mandatory method called when the form is submitted */
    onSubmit: PropTypes.func.isRequired,
    /** Key to associate to the search input */
    paramKey: PropTypes.string,
    /** A placeholder for the input */
    placeholder: PropTypes.string,
    /** Current value of the search input */
    query: PropTypes.string,
    /** Optional method that returns a string or HTML to display a
     * section title (for adaptive design, not displayed) */
    renderSectionTitle: PropTypes.func,
    /** Optional method that returns a string or HTML representing
     * a single suggestion item */
    renderSuggestion: PropTypes.func.isRequired,
    /** The array of suggestions */
    suggestions: PropTypes.array.isRequired,
  };

  static defaultProps = {
    buttonText: 'Rechercher',
    labelKey: 'label',
    locale: CaligramReact.config().locale,
    minLength: 2,
    paramKey: 'q',
    placeholder: null,
    query: '',
    renderSectionTitle: (section) => {
      return section.meta.type;
    },
  };

  onSubmit = (event) => {
    event.preventDefault();
    this.props.onSubmit(event);
    return false;
  };

  getSectionSuggestions = (section) => {
    return section.data;
  };

  getSuggestionValue = (suggestion) => {
    return suggestion[this.props.labelKey];
  };

  shouldRenderSuggestions = (value) => {
    return value.trim().length > this.props.minLength;
  };

  render() {
    const {
      buttonText,
      locale,
      minLength,
      onChange,
      onClear,
      onFetch,
      onSelect,
      paramKey,
      placeholder,
      query,
      renderSectionTitle,
      renderSuggestion,
      suggestions,
    } = this.props;

    const inputProps = {
      name: paramKey,
      onChange,
      placeholder: placeholder || t(locale, 'search.placeholder'),
      value: query,
    };

    const theme = {
      container: 'container',
      containerOpen: 'containerOpen',
      input: 'input',
      sectionContainer: 'sectionContainer',
      sectionTitle: 'sectionTitle',
      suggestion: 'suggestion',
      suggestionHighlighted: 'suggestionFocused',
      suggestionsContainer: 'suggestionsContainer',
      suggestionsList: 'suggestionsList',
    };

    return (
      <form className="SearchBar" onSubmit={this.onSubmit} method="get">
        <div className="icon-wrapper">
          <div className="icon">
            <Icon name="search" />
          </div>
          <Autosuggest
            suggestions={suggestions}
            multiSection
            onSuggestionSelected={onSelect}
            onSuggestionsFetchRequested={onFetch}
            onSuggestionsClearRequested={onClear}
            getSuggestionValue={this.getSuggestionValue}
            getSectionSuggestions={this.getSectionSuggestions}
            renderSuggestion={renderSuggestion}
            renderSectionTitle={renderSectionTitle}
            shouldRenderSuggestions={this.shouldRenderSuggestions}
            alwaysRenderSuggestions={minLength === 0}
            inputProps={inputProps}
            theme={theme}
          />
        </div>
        <button type="submit">{ buttonText }</button>
      </form>
    );
  }
}
