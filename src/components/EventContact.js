import React from 'react';
import PropTypes from 'prop-types';

const EventContact = ({ name, phone, email, url }) => {
  const contactInfo = [];

  if (name) contactInfo.push({ key: 'name', element: name });
  if (phone) contactInfo.push({ key: 'phone', element: <a href={`tel:+1-${phone}`}>{phone}</a> });
  if (email) contactInfo.push({ key: 'email', element: <a href={`mailto:${email}`}>{email}</a> });
  if (url) contactInfo.push({ key: 'url', element: <a href={url} target="_blank" rel="noopener noreferrer">{url}</a> });

  if (contactInfo.length === 0) {
    return null;
  }

  return (
    <div className="EventContact">
      <ul>
        { contactInfo.map(info => <li key={info.key}>{info.element}</li>) }
      </ul>
    </div>
  );
};

EventContact.propTypes = {
  /** The contact's name. */
  name: PropTypes.string,
  /** The contact's phone number. */
  phone: PropTypes.string,
  /** The contact's email address. */
  email: PropTypes.string,
  /** The contact's website. */
  url: PropTypes.string,
};

EventContact.defaultProps = {
  name: null,
  phone: null,
  email: null,
  url: null,
};

export default EventContact;
