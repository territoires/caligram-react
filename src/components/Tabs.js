import React from 'react';
import PropTypes from 'prop-types';
import Tab from './Tab';

function renderChildren(children, selected, small) {
  return React.Children.map(children, (child) => {
    if (child.type === Tab) {
      return React.cloneElement(child, {
        small,
        selected: selected === child.key,
      });
    }
    return child;
  });
}

const Tabs = ({ children, selected, small }) => {
  return <ul className="Tabs">{renderChildren(children, selected, small)}</ul>;
};

Tabs.propTypes = {
  children: PropTypes.node,
  selected: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  small: PropTypes.bool,
};

Tabs.defaultProps = {
  children: null,
  selected: null,
  small: false,
};

export default Tabs;
