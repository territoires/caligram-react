import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import EventDetailClassicLayout from './EventDetailClassicLayout';
import EventDetailModernLayout from './EventDetailModernLayout';
import CaligramReact from '../config';

const EventDetail = (props) => {
  const {
    audienceAction,
    children,
    groupAction,
    tagAction,
    backButtonAction,
    event,
    layout,
    locale,
    mapApiKey,
    selectedDate: selectedDateProp,
    typeAction,
  } = props;

  const selectedDateString = selectedDateProp || moment(event.dates[0].start_date).format('YYYY-MM-DD_HH-mm-ss');
  const selectedDate = event.dates.find((date) => {
    return moment(date.start_date).format('YYYY-MM-DD_HH-mm-ss') === selectedDateString;
  });

  const layoutProps = {
    audienceAction,
    backButtonAction,
    groupAction,
    contact: {
      name: event.contact_name,
      email: event.contact_email,
      phone: event.contact_phone,
      url: event.contact_url,
    },
    event,
    layout,
    locale,
    price: {
      type: event.price_type,
      notes: event.price_notes,
      prices: event.prices,
    },
    map: {
      latitude: event.venue && event.venue.latitude,
      longitude: event.venue && event.venue.longitude,
      apiKey: mapApiKey,
    },
    selectedDate,
    tagAction,
    typeAction,
  };

  if (children) {
    return children(layoutProps);
  }

  switch (layout) {
    case 'modern':
      return <EventDetailModernLayout {...layoutProps} />;
    default:
      return <EventDetailClassicLayout {...layoutProps} />;
  }
};

EventDetail.propTypes = {
  /** Action triggered when the Back button is clicked */
  backButtonAction: PropTypes.func,
  /** Optional child elements to be rendered instead of built-in layouts */
  children: PropTypes.func,
  /** Event object in the Caligram API format. */
  event: PropTypes.object.isRequired,
  /** The layout to use */
  layout: PropTypes.string,
  /** The Google Static Maps API key. */
  mapApiKey: PropTypes.string.isRequired,
  /** The selected date as used in the query string. */
  selectedDate: PropTypes.string,
  /** Function called when an audience is clicked. */
  audienceAction: PropTypes.func,
  /** Function called when a group is clicked. */
  groupAction: PropTypes.func,
  /** Function called when a tag is clicked. */
  tagAction: PropTypes.func,
  /** Function called when a type is clicked. */
  typeAction: PropTypes.func,
};

EventDetail.defaultProps = {
  audienceAction: () => {},
  backButtonAction: () => {},
  children: null,
  groupAction: () => {},
  layout: 'classic',
  locale: CaligramReact.config().locale,
  selectedDate: null,
  tagAction: () => {},
  typeAction: () => {},
};

export default EventDetail;
