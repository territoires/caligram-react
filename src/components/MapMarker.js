import React, { Component } from 'react';
import PropTypes from 'prop-types';
import EventList from './EventList';
import EventListItem from './EventListItem';
import Icon from './Icon';
import Popover from './Popover';
import { handleKeyPress } from '../helpers/utils';

export default class MapMarker extends Component {
  static propTypes = {
    /** Optional array to display more than one event at a time */
    events: PropTypes.array,
  };

  static defaultProps = {
    events: [],
  };

  state = {
    popoverVisible: false,
  };

  setPopoverVisibility(status) {
    this.setState({ popoverVisible: status });
  }

  showPopover() {
    if (!this.state.popoverVisible) {
      this.setPopoverVisibility(true);
    }
  }

  hidePopover() {
    if (this.state.popoverVisible) {
      this.setPopoverVisibility(false);
    }
  }

  render() {
    return (
      this.props.id || this.props.events.length // eslint-disable-line react/prop-types
        ? (
          <span
            onMouseEnter={this.showPopover.bind(this)}
            onMouseLeave={this.hidePopover.bind(this)}
            onClick={this.showPopover.bind(this)}
            onKeyPress={handleKeyPress(this.showPopover.bind(this))}
            className="MapMarker interactive"
          >
            <Popover
              visible={this.state.popoverVisible}
              clickOverlay={this.hidePopover.bind(this)}
              width={200}
              dropdownView={
                this.props.events.length
                  ? <EventList events={this.props.events} layout="grid" size="small" groupBy="none" columns={1} />
                  : <EventListItem layout="grid" size="small" event={this.props} />
              }
            >
              <Icon name="pin" />
            </Popover>
          </span>
        )
        : <span className="MapMarker"><Icon name="pin" /></span>
    );
  }
}
