import React from 'react';
import PropTypes from 'prop-types';

const Link = ({ to, onClick, children, target }) => {
  let fullPath = to.pathname;
  if (to.query) {
    fullPath += `?${Object.keys(to.query).map((k) => {
      return `${encodeURIComponent(k)}=${encodeURIComponent(to.query[k])}`;
    }).join('&')}`;
  }
  return <a href={fullPath} onClick={onClick} target={target}>{children}</a>;
};

Link.propTypes = {
  /** The contents of the link. */
  children: PropTypes.node.isRequired,
  /** Object specifying where the link should point to; based on react-router Link component */
  to: PropTypes.shape({
    pathname: PropTypes.string.isRequired,
    query: PropTypes.object,
  }).isRequired,
  /** onClick action to be passed on to the link */
  onClick: PropTypes.func,
  target: PropTypes.string,
};

Link.defaultProps = {
  onClick: () => {},
  target: null,
};

export default Link;
