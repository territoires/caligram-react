import React, { Component } from 'react';
import PropTypes from 'prop-types';
import GoogleMapReact from 'google-map-react';
import MapMarker from './MapMarker';

export default class DynamicMap extends Component {
  static DefaultMarker = () => {
    return <MapMarker />;
  }

  static propTypes = {
    /** The Google Maps API key */
    apiKey: PropTypes.string.isRequired,
    /** The starting latitude. */
    centerLat: PropTypes.number.isRequired,
    /** The starting longitude. */
    centerLng: PropTypes.number.isRequired,
    /** Order in which events are drawn. Used to manage z-indices. */
    drawDirection: PropTypes.oneOf(['NW', 'NE', 'SW', 'SE']),
    /** A list of events to render. */
    events: PropTypes.array.isRequired,
    /** A dynamic, overriding latitude as provided by the Geolocation API, for example */
    geoLatitude: PropTypes.number,
    /** A dynamic, overriding longitude as provided by the Geolocation API, for example */
    geoLongitude: PropTypes.number,
    /** Method called when clicking on a marker. */
    handleOnClick: PropTypes.func,
    /** The starting zoom. */
    initialZoom: PropTypes.number.isRequired,
    /** Configuration object passed to the underlying maps API. */
    mapOptions: PropTypes.object,
    /** A component or node to display as a marker. */
    marker: PropTypes.func,
    /** Method called when the map has moved.
     * It is recommended to fetch as far as the largest dimension
     * from the borders to ensure there are no gaps when dragging. */
    onCenterChange: PropTypes.func,
  };

  static defaultProps = {
    drawDirection: 'NW',
    geoLatitude: null,
    geoLongitude: null,
    handleOnClick: null,
    onCenterChange: () => {},
    mapOptions: {
      clickableIcons: false,
    },
    marker: DynamicMap.DefaultMarker,
  };

  state = {
    clusterFactor: 10000,
  }

  handleOnCenterChange({ center, zoom, bounds, marginBounds }) {
    let newFactor = 10000;

    // TODO Find an algorithm
    if (zoom < 6) {
      newFactor = 0.1;
    } else if (zoom < 12) {
      newFactor = 100;
    } else if (zoom < 14) {
      newFactor = 250;
    } else if (zoom < 16) {
      newFactor = 1000;
    }

    this.setState({
      clusterFactor: newFactor,
    });

    this.props.onCenterChange({
      center,
      zoom,
      bounds,
      marginBounds,
    });
  }

  render() {
    const newCenter = {
      lat: this.props.geoLatitude || this.props.centerLat,
      lng: this.props.geoLongitude || this.props.centerLng,
    };

    const {
      apiKey,
      drawDirection,
      events,
      handleOnClick,
      initialZoom,
      mapOptions,
    } = this.props;
    const Marker = this.props.marker;

    const { clusterFactor } = this.state;

    const clusteredEvents = events.reduce((acc, ev) => {
      const clusteredLat = Math.round(ev.lat * clusterFactor) / clusterFactor;
      const clusteredLng = Math.round(ev.lng * clusterFactor) / clusterFactor;
      const key = `${clusteredLat}|${clusteredLng}`;

      if (!acc[key]) {
        acc[key] = []; // eslint-disable-line no-param-reassign
      }
      acc[key].push(ev);

      return acc;
    }, {});
    const eventsClusters = Object.keys(clusteredEvents).map((key) => {
      if (clusteredEvents[key].length > 1) {
        const meanLat = clusteredEvents[key].reduce((acc, ev) => {
          return acc + ev.lat;
        }, 0) / clusteredEvents[key].length;
        const meanLng = clusteredEvents[key].reduce((acc, ev) => {
          return acc + ev.lng;
        }, 0) / clusteredEvents[key].length;

        return {
          id: clusteredEvents[key].reduce((acc, ev) => {
            if (acc === '') {
              return `${ev.id}`;
            }

            return `${acc}-${ev.id}`;
          }, ''),
          lat: meanLat,
          lng: meanLng,
          events: clusteredEvents[key],
        };
      }

      return clusteredEvents[key][0];
    });

    const orderedEvents = eventsClusters.sort(function (a, b) {
      const latDiff = b.lat - a.lat;
      const lngDiff = b.lng - a.lng;

      const lat = latDiff > 0 ? 1 : -1;
      const lng = lngDiff > 0 ? 1 : -1;

      return (drawDirection[0] === 'N' ? -lat : lat)
        || (drawDirection[1] === 'E' ? -lng : lng);
    });

    return (
      <div className="DynamicMap">
        <GoogleMapReact
          bootstrapURLKeys={{
            key: apiKey,
            v: '3.30',
          }}
          center={newCenter}
          defaultZoom={initialZoom}
          onChange={this.handleOnCenterChange.bind(this)}
          options={mapOptions}
          style={{ minHeight: '1px' }} // The map does not always display without this
        >
          {orderedEvents.map((e) => {
            return (
              <Marker key={`${e.id}-${e.start_date}`} onClick={handleOnClick.bind(this, e.id)} {...e} />
            );
          })}
        </GoogleMapReact>
      </div>
    );
  }
}
