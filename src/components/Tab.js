import React from 'react';
import PropTypes from 'prop-types';
import Button from './Button';

const Tab = ({ action, icon, selected, small, title }) => {
  const newAction = selected ? null : action;
  const type = selected ? 'primary' : 'secondary';
  const newIcon = icon || null;
  return <Button className="Tab" action={newAction} iconLeft={newIcon} title={title} small={small} type={type} />;
};

Tab.propTypes = {
  /** Action triggered when the tab item is clicked. */
  action: PropTypes.func,
  /** The name of the icon to display on the left side of the tab item. */
  icon: PropTypes.string,
  /** Whether the tab item is selected. */
  selected: PropTypes.bool,
  /** Whether the tab has smaller text. */
  small: PropTypes.bool,
  /** The title of the tab item. */
  title: PropTypes.string.isRequired,
};

Tab.defaultProps = {
  action: null,
  icon: null,
  selected: false,
  small: false,
};

export default Tab;
