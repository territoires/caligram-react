import React, { Component } from 'react';
import PropTypes from 'prop-types';
import EventImage from './EventImage';

class EventMedia extends Component {
  static propTypes = {
    images: PropTypes.array,
    layout: PropTypes.string,
  };

  static defaultProps = {
    images: [],
    layout: 'plain',
  };

  constructor(props) {
    super(props);
    this.state = {
      currentImage: 0,
    };
  }

  render() {
    const { images, layout } = this.props;

    if (images.length) {
      switch (layout) {
        case 'carousel': {
          const currentImage = images[this.state.currentImage];

          return (
            <div className={`EventMedia ${layout}`}>
              <div className="current">
                <EventImage
                  src={currentImage.sizes.large}
                  alt={currentImage.description}
                  credit={currentImage.credits}
                />
              </div>
              { images.length > 1 && (
                <nav>
                  { images.map((image, idx) => {
                    const handleClick = () => {
                      this.setState({ currentImage: idx });
                    };
                    const className = this.state.currentImage === idx ? 'current' : '';

                    return (
                      <button type="button" key={`image-${image.id}`} onClick={handleClick} className={className}>
                        <EventImage src={image.sizes.thumbnail} alt={image.description} />
                      </button>
                    );
                  }) }
                </nav>
              )}
            </div>
          );
        }
        default:
          return (
            <div className={`EventMedia ${layout}`}>
              { images.map(image => <EventImage key={`image-${image.id}`} src={image.sizes.thumbnail} alt={image.description} credit={image.credits} />) }
            </div>
          );
      }
    }

    return null;
  }
}

export default EventMedia;
