import React from 'react';
import PropTypes from 'prop-types';
import { classNames } from '../helpers/utils';
import Icon from './Icon';

const Button = (props) => {
  const {
    title,
    className,
    href,
    action,
    disabled,
    hideTitle,
    iconLeft,
    iconRight,
    newWindow,
    elementRef,
    small,
    type,
    width,
  } = props;

  const newProps = {
    className: classNames('Button', [className, type, width], { hideTitle, iconLeft, iconRight, small }),
  };

  if (elementRef) {
    newProps.ref = elementRef;
  }

  if (hideTitle) {
    newProps['aria-label'] = title;
    newProps.title = title;
  }

  const content = (
    <span>
      { iconLeft && <Icon name={iconLeft} /> }
      { !hideTitle && title }
      { iconRight && <Icon name={iconRight} /> }
    </span>
  );

  if (href) {
    if (newWindow) {
      newProps.target = '_blank';
      newProps.rel = 'noopener noreferrer';
    }
    return <a href={href} {...newProps}>{content}</a>;
  }

  return <button onClick={action} disabled={disabled} type="button" {...newProps}>{content}</button>;
};

Button.propTypes = {
  /** The title of the button. */
  title: PropTypes.string.isRequired,
  /** The CSS class of the block. */
  className: PropTypes.string,
  /** URL to link to. */
  href: PropTypes.string,
  /** Action triggered when the button is clicked. */
  action: PropTypes.func,
  /** Whether the button is disabled. */
  disabled: PropTypes.bool,
  /** Whether the title should be hidden (if so, it will still be available to screenreaders) */
  hideTitle: PropTypes.bool,
  /** The name of the icon to display on the left side of the button. */
  iconLeft: PropTypes.string,
  /** The name of the icon to display on the right side of the button. */
  iconRight: PropTypes.string,
  /** Whether to open the link in a new window. */
  newWindow: PropTypes.bool,
  /** Passthrough for the ref special prop. https://fb.me/react-special-props */
  elementRef: PropTypes.func,
  /** Whether the button has smaller text. */
  small: PropTypes.bool,
  /** The button type. */
  type: PropTypes.oneOf(['primary', 'secondary', 'borderless']),
  /** The button width. */
  width: PropTypes.oneOf(['full', 'fit']),
};

Button.defaultProps = {
  action: null,
  className: null,
  disabled: false,
  elementRef: null,
  hideTitle: false,
  href: null,
  iconLeft: null,
  iconRight: null,
  newWindow: false,
  small: false,
  type: 'secondary',
  width: 'fit',
};

export default Button;
