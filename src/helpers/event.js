export function hasContact(event) {
  return event.contact_name || event.contact_phone || event.contact_email || event.contact_url;
}

export function hasPrice(event) {
  return event.price_type || event.price_notes || event.prices.length > 0;
}
