import CaligramReact from '../config';

const config = CaligramReact.config();

export default function t(locale, key, count = 1) {
  const translations = config.locales[locale];
  const nestedKeys = key.split('.');
  let nextObject = translations;
  let nextKey;

  if (!translations) {
    throw new Error(`Locale '${locale}' does not exist in caligram-react config`);
  }

  while (nestedKeys.length > 0) {
    nextKey = nestedKeys.shift();
    nextObject = nextObject[nextKey];
  }

  if (typeof nextObject !== 'string') {
    throw new Error(`Key '${key}' does not exist for locale '${locale}' in caligram-react config`);
  }

  const translation = nextObject;
  const pluralForms = translation.split(' |||| ');

  if (pluralForms.length > 1) {
    if (count > 1) {
      return pluralForms[1];
    }
    return pluralForms[0];
  }

  return translation;
}
