export function isObject(item) {
  return (item && typeof item === 'object' && !Array.isArray(item));
}

export function deepMerge(target, ...sources) {
  if (!sources.length) return target;
  const source = sources.shift();

  if (isObject(target) && isObject(source)) {
    Object.keys(source).forEach(function (key) {
      if (isObject(source[key])) {
        if (!target[key]) Object.assign(target, { [key]: {} });
        deepMerge(target[key], source[key]);
      } else {
        Object.assign(target, { [key]: source[key] });
      }
    });
  }

  return deepMerge(target, ...sources);
}

// Taken from https://www.npmjs.com/package/classnames
export function classNames(...args) {
  const classes = [];

  for (let i = 0; i < args.length; i += 1) {
    const arg = args[i];

    if (arg) {
      const argType = typeof arg;

      if (argType === 'string' || argType === 'number') {
        classes.push(arg);
      } else if (Array.isArray(arg) && arg.length) {
        const inner = classNames(...arg);
        if (inner) {
          classes.push(inner);
        }
      } else if (argType === 'object') {
        Object.keys(arg).forEach((key) => {
          if ({}.hasOwnProperty.call(arg, key) && arg[key]) {
            classes.push(key);
          }
        });
      }
    }
  }

  return classes.join(' ');
}

export function handleKeyPress() {
  // console.log('handleKeyPress stub with action', action);
}

export function getPathFromUrl(url) {
  if (!url) {
    return null;
  }

  const matches = url.match(/^((http[s]?|ftp):\/)?\/?([^:/\s]+)((\/\w+)*\/)([\w\-.]+[^#?\s]+)(.*)?(#[\w-]+)?$/);

  if (matches[4]) {
    return matches[4] + matches[6];
  }

  return null;
}
