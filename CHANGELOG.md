# Changelog

## [0.14.2] - 2020-12-08
### Fixed
- `EventVenue` no longer displays `0` when given no address

## [0.14.1] - 2020-12-07
### Changed
- `StaticMap` now displays nothing when given null coordinates

### Fixed
- `EventVenue` no longer displays `0` when given null coordinates

## [0.14.0] - 2020-10-16
### Added
- `EventDetail` now shows Facebook event link

## [0.13.1] - 2020-05-26
### Added
- `EventVenue` now allows a custom directions link through the `link` prop.

## [0.13.0] - 2020-04-15
### Added
- `EventList` and `EventListItem` now accept a `target` prop to change link behaviour

## [0.12.4] - 2020-04-09
### Changed
- ShareLinks now uses a relative URL for the ics link

## [0.12.3] - 2019-11-07
### Changed
- When using a custom template, `EventList` no longer wraps `EventListItem` in a `<div>`, allowing easier CSS customization in certain cases.

## [0.12.2] - 2019-11-01
### Fixed
- `EventList` passes locale to `Pagination`

## [0.12.1] - 2019-10-29
### Fixed
- `EventMedia` only shows thumbnails when there are multiple images

## [0.12.0] - 2019-10-29
### Added
- New `EventMedia` component with multiple image support
- `EventImage` has a new `credit` prop

## [0.11.2] - 2019-09-13
### Added
- `EventVenue` has a new `linkOnVenueName` prop
- `EventDate` has a new `notes` prop

## [0.11.1] - 2019-07-08
### Fixed
- `EventList` now uses localized date format instead of hard-coded format

## [0.11.0] - 2019-07-07
### Fixed
- Uniformize localization
- Localize incompletely localized components

## [0.10.4] - 2019-06-28
### Fixed
- `Col` now displays properly for all column combinations
- `EventDetail` no longer warns when providing a function as `children`

## [0.10.3] - 2019-05-06
### Changed
- `SelectionList` now filters by group in case of ID conflict

## [0.10.2] - 2019-05-06
### Changed
- `EventDate` now hides the time when start time is midnight and duration is undefined

## [0.10.1] - 2019-05-04
### Added
- `SelectionList` now includes group in the key to allow multiple items with the same id belonging to different groups

## [0.10.0] - 2019-05-04
### Added
- Added `Grid` and `Col` components

### Fixed
- Clear button in ActiveFilters now displays correctly

## [0.9.0] - 2019-03-15
### Added
- Added alternate layout for `EventDetail`

### Changed
- Improved accessibility for `EventDate`, `SelectableLabel`, `Checkbox`

## [0.8.2] - 2019-01-11
### Fixed
- `EventListItem` now correctly shows images when using the `carousel` layout

## [0.8.1] - 2019-01-11
### Added
- `EventList`'s `layout` prop now has a `carousel` option

## [0.8.0] - 2018-11-07
### Changed
- `EventListItem` has a new prop `LinkDate` and the type of `DateFormat` is changed.
- `EventListItem`'s `LinkDate` allows to select the key used for the `Link` element `date` query
- `EventListItem`'s `DateFormat` is now a method allowing more flexibility in the format.
- `EventListItem`'s `featured` prop is deprecated.

## [0.7.2] - 2018-09-20
### Fixed
- `EventMeta` uses provided `DocumentMeta` instance

## [0.7.1] - 2018-09-05
### Fixed
- `Calendar` now displays single-letter abbreviations for weekdays

## [0.7.0] - 2018-08-28
### Changed
- Updated for React 16
- No longer depends on the Link component from `react-router`. A default router-agnostic `Link` component is now supplied, which can be overridden via the `linkComponent` configuration option.
- `EventVenue` now shows the room name immediately after the venue name

## [0.6.8] - 2018-07-25
### Added
- `EventDetail` passes the `room` property to EventVenue by default

## [0.6.7] - 2018-07-25
### Added
- `EventVenue` now supports a `room` property, an object providing a `name` key

## [0.6.6] - 2018-06-07
### Changed
- `EventListItem` now uses `display_image_url` instead of `thumbnail_url`

### Fixed
- Added style overrides for `SearchBar`

## [0.6.5] - 2018-06-06
### Added
- `EventDetail` now includes linked organizations

### Fixed
- `ShareLinks` no longer causes a fatal error when event URL is missing `http://`

## [0.6.4] - 2018-05-30
### Fixed
- `ShareLinks` now fails gracefully when event doesn’t have a URL

## [0.6.3] - 2018-05-23
### Changed
- Added CSS rules to override more cases of pollution from external stylesheets

## [0.6.2] - 2018-05-11
### Added
- `EventList` has new `beforeFilters` and `beforeEvents` props to allow injection of custom content

## [0.6.1] - 2018-05-10
### Added
- New `mobile-only` mixin
- New `linkedIn` option for `ShareLinks`

### Changed
- `Button` now has an optional `hideTitle` prop for icon-only button (with the title preserved for screen readers)
- `ShareLinks` now has a `noTint` option to prevent icon tinting when needed
- `Page` replaces the `customLayout` prop with `layout`, which accepts values `narrow`, `wide`, and `custom`

### Fixed
- Dates popover in `EventDate` now correctly renders on top of other elements

## [0.6.0] - 2018-03-28
### Added
- New components: Checkbox, Tabs and Tab

### Changed
- ShareLinks was extended to allow printing, as well as Google, Yahoo, and iCal links
- DynamicMap: update style of events in popover
- MapMarker has been generalized to be used uniformly in StaticMap and DynamicMap
- EventListItem: add `size` prop to allow small grid items
- HeadingBlock has a new `inline` prop

### Fixed
- DynamicMap: popover uses the correct font rather than the Google-supplied one

## [0.5.3] - 2018-02-21
### Added
- EventList : add optional tile
- EventDetail : use alternate date key when present

## [0.5.2] - 2018-02-21
### Added
- MapMarker: fix marker position and click zone as well as mobile popup dimensions

## [0.5.1] - 2018-02-14
### Added
- DynamicMap: close markers are now clustered together

## [0.5.0] - 2018-02-14
### Added
- GeolocatedDynamicMap: a component providing a geolocated DynamicMap

## [0.4.19] - 2018-02-09
### Fixed
- Fix MapMarker draw order to avoid z-index issues

## [0.4.18] - 2018-01-31
### Fixed
- Allow opening and closing map marker on mobile, would only support hover events

## [0.4.17] - 2018-01-31
### Fixed
- Fix delayed load

## [0.4.16] - 2018-01-31
### Fixed
- Export MapMarker for external reuse

## [0.4.15] - 2018-01-31
### Fixed
- PropTypes source in Button

## [0.4.14] - 2018-01-31
### Added
- MapMarker component, to be used with DynamicMap

## [0.4.13] - 2018-01-30
### Changed
- Harden styles to prevent overrides by client CSS

## [0.4.12] - 2018-01-25
### Changed
- Improve styles for tablet version

## [0.4.11] - 2018-01-23
### Changed
- Change `SearchBar` styles to work better with white backgrounds and narrow columns

## [0.4.10] - 2018-01-18
### Changed
- Minor style changes in `Pagination` and `EventList`

## [0.4.9] - 2018-01-14
### Fixed
- Actually correctly clear floats in `EventList`
- Fix missing filters in certain stories

## [0.4.8] - 2018-01-14
### Fixed
- Correctly clear floats in `EventList`

## [0.4.7] - 2018-01-03
### Fixed
- Avoid React DOM validation error of &lt;a&gt; in &lt;a&gt; in `SelectionList`

## [0.4.6] - 2017-12-18
### Added
- Allow custom `className` for `EventList`

## [0.4.5] - 2017-12-14
### Fixed
- Fix broken grid layout

## [0.4.4] - 2017-12-13
### Added
- Allow custom icons for SelectionList via new `iconSelected` and `iconUnselected` props

## [0.4.3] - 2017-12-06
### Fixed
- Types, tags and audiences now show up correctly in `EventDetail`

## [0.4.2] - 2017-11-30
### Fixed
- Use `label` as the default labelKey for `ActiveFilters` and `SelectionList`. Fixes label-less filters.

## [0.4.1] - 2017-11-29
### Added
- New $border-radius variable, including changes to override new Chrome default for buttons

## [0.4.0] - 2017-11-28
### Added
- Optional Back button in `Page` and `EventDetail` components
- New `borderless` type for `Button` component

### Changed
- CSS now survives an inherited `text-align: center` rule

## [0.3.2] - 2017-11-27
### Fixed
- Use official version of SVGR.

## [0.3.1] - 2017-11-22
### Changed
- SVG icons are now pre-converted to React components

## [0.3.0] - 2017-10-12
### Added
- New components: `Button` and `DynamicMap`
- `Icon` component can use custom icons
- New optional `icon` prop in `HeadingBlock`
- New custom SVG map marker in `StaticMap`
- Show `ticket_url` property as a button in `EventDetail`
- Initial i18n support

### Changed
- `EventDate` omits minute zero when displaying the time

## [0.2.2] - 2017-09-14
### Fixed
- Update `SearchBar` component to use `$border-width` variable
- Center pagination arrows

## [0.2.1] - 2017-08-30
### Fixed
- Use correct font size for event description

## [0.2.0] - 2017-08-30
### Added
- Extend `EventDetail` to allow custom actions when clicking types, tags and audiences
- New Sass `$border-width` variable
- New Sass mixins for responsive text sizes and links

### Changed
- Replace the `ItemList` component with an updated `SelectionList`
- Lots of minor style adjustments

## [0.1.3] - 2017-09-21
### Added
- Show `ticket_url` property in `EventDetail`

## [0.1.2] - 2017-09-21
Let’s say this one never existed.

## [0.1.1] - 2017-07-27
### Fixed
- Include SCSS files in npm package, so that it actually works.

## 0.1.0 - 2017-07-27
### Added
- Initial release.

## Notes
The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

[0.1.1]: https://gitlab.com/territoires/caligram-react/compare/v0.1.0...v0.1.1
[0.1.2]: https://gitlab.com/territoires/caligram-react/compare/v0.1.1...v0.1.2
[0.1.3]: https://gitlab.com/territoires/caligram-react/compare/v0.1.2...v0.1.3
[0.2.0]: https://gitlab.com/territoires/caligram-react/compare/v0.1.1...v0.2.0
[0.2.1]: https://gitlab.com/territoires/caligram-react/compare/v0.2.0...v0.2.1
[0.2.2]: https://gitlab.com/territoires/caligram-react/compare/v0.2.1...v0.2.2
[0.3.0]: https://gitlab.com/territoires/caligram-react/compare/v0.2.2...v0.3.0
[0.3.1]: https://gitlab.com/territoires/caligram-react/compare/v0.3.0...v0.3.1
[0.3.2]: https://gitlab.com/territoires/caligram-react/compare/v0.3.1...v0.3.2
[0.4.0]: https://gitlab.com/territoires/caligram-react/compare/v0.3.2...v0.4.0
[0.4.1]: https://gitlab.com/territoires/caligram-react/compare/v0.4.0...v0.4.1
[0.4.2]: https://gitlab.com/territoires/caligram-react/compare/v0.4.1...v0.4.2
[0.4.3]: https://gitlab.com/territoires/caligram-react/compare/v0.4.2...v0.4.3
[0.4.4]: https://gitlab.com/territoires/caligram-react/compare/v0.4.3...v0.4.4
[0.4.5]: https://gitlab.com/territoires/caligram-react/compare/v0.4.4...v0.4.5
[0.4.6]: https://gitlab.com/territoires/caligram-react/compare/v0.4.5...v0.4.6
[0.4.7]: https://gitlab.com/territoires/caligram-react/compare/v0.4.6...v0.4.7
[0.4.8]: https://gitlab.com/territoires/caligram-react/compare/v0.4.7...v0.4.8
[0.4.9]: https://gitlab.com/territoires/caligram-react/compare/v0.4.8...v0.4.9
[0.4.10]: https://gitlab.com/territoires/caligram-react/compare/v0.4.9...v0.4.10
[0.4.11]: https://gitlab.com/territoires/caligram-react/compare/v0.4.10...v0.4.11
[0.4.12]: https://gitlab.com/territoires/caligram-react/compare/v0.4.11...v0.4.12
[0.4.13]: https://gitlab.com/territoires/caligram-react/compare/v0.4.12...v0.4.13
[0.4.14]: https://gitlab.com/territoires/caligram-react/compare/v0.4.13...v0.4.14
[0.4.15]: https://gitlab.com/territoires/caligram-react/compare/v0.4.14...v0.4.15
[0.4.16]: https://gitlab.com/territoires/caligram-react/compare/v0.4.15...v0.4.16
[0.4.17]: https://gitlab.com/territoires/caligram-react/compare/v0.4.16...v0.4.17
[0.4.18]: https://gitlab.com/territoires/caligram-react/compare/v0.4.17...v0.4.18
[0.4.19]: https://gitlab.com/territoires/caligram-react/compare/v0.4.18...v0.4.19
[0.5.0]: https://gitlab.com/territoires/caligram-react/compare/v0.4.19...v0.5.0
[0.5.1]: https://gitlab.com/territoires/caligram-react/compare/v0.5.0...v0.5.1
[0.5.2]: https://gitlab.com/territoires/caligram-react/compare/v0.5.1...v0.5.2
[0.5.3]: https://gitlab.com/territoires/caligram-react/compare/v0.5.2...v0.5.3
[0.6.0]: https://gitlab.com/territoires/caligram-react/compare/v0.5.3...v0.6.0
[0.6.1]: https://gitlab.com/territoires/caligram-react/compare/v0.6.0...v0.6.1
[0.6.2]: https://gitlab.com/territoires/caligram-react/compare/v0.6.1...v0.6.2
[0.6.3]: https://gitlab.com/territoires/caligram-react/compare/v0.6.2...v0.6.3
[0.6.4]: https://gitlab.com/territoires/caligram-react/compare/v0.6.3...v0.6.4
[0.6.5]: https://gitlab.com/territoires/caligram-react/compare/v0.6.4...v0.6.5
[0.6.6]: https://gitlab.com/territoires/caligram-react/compare/v0.6.5...v0.6.6
[0.6.7]: https://gitlab.com/territoires/caligram-react/compare/v0.6.6...v0.6.7
[0.6.8]: https://gitlab.com/territoires/caligram-react/compare/v0.6.7...v0.6.8
[0.7.0]: https://gitlab.com/territoires/caligram-react/compare/v0.6.8...v0.7.0
[0.7.1]: https://gitlab.com/territoires/caligram-react/compare/v0.7.0...v0.7.1
[0.7.2]: https://gitlab.com/territoires/caligram-react/compare/v0.7.1...v0.7.2
[0.8.0]: https://gitlab.com/territoires/caligram-react/compare/v0.7.2...v0.8.0
[0.8.1]: https://gitlab.com/territoires/caligram-react/compare/v0.8.0...v0.8.1
[0.8.2]: https://gitlab.com/territoires/caligram-react/compare/v0.8.1...v0.8.2
[0.9.0]: https://gitlab.com/territoires/caligram-react/compare/v0.8.2...v0.9.0
[0.10.0]: https://gitlab.com/territoires/caligram-react/compare/v0.9.0...v0.10.0
[0.10.1]: https://gitlab.com/territoires/caligram-react/compare/v0.10.0...v0.10.1
[0.10.2]: https://gitlab.com/territoires/caligram-react/compare/v0.10.1...v0.10.2
[0.10.3]: https://gitlab.com/territoires/caligram-react/compare/v0.10.2...v0.10.3
[0.10.4]: https://gitlab.com/territoires/caligram-react/compare/v0.10.3...v0.10.4
[0.11.0]: https://gitlab.com/territoires/caligram-react/compare/v0.10.4...v0.11.0
[0.11.1]: https://gitlab.com/territoires/caligram-react/compare/v0.11.0...v0.11.1
[0.11.2]: https://gitlab.com/territoires/caligram-react/compare/v0.11.1...v0.11.2
[0.12.0]: https://gitlab.com/territoires/caligram-react/compare/v0.11.2...v0.12.0
[0.12.1]: https://gitlab.com/territoires/caligram-react/compare/v0.12.0...v0.12.1
[0.12.2]: https://gitlab.com/territoires/caligram-react/compare/v0.12.1...v0.12.2
[0.12.3]: https://gitlab.com/territoires/caligram-react/compare/v0.12.2...v0.12.3
[0.12.4]: https://gitlab.com/territoires/caligram-react/compare/v0.12.3...v0.12.4
[0.13.0]: https://gitlab.com/territoires/caligram-react/compare/v0.12.4...v0.13.0
[0.13.1]: https://gitlab.com/territoires/caligram-react/compare/v0.13.0...v0.13.1
[0.14.0]: https://gitlab.com/territoires/caligram-react/compare/v0.13.1...v0.14.0
[0.14.1]: https://gitlab.com/territoires/caligram-react/compare/v0.14.0...v0.14.1
[0.14.2]: https://gitlab.com/territoires/caligram-react/compare/v0.14.1...v0.14.2
