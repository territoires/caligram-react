# caligram-react

React components for [Caligram](http://pro.caligram.com).

## Demo and documentation

The [Storybook](http://territoires.gitlab.io/caligram-react/) shows every component in action, along with documentation.

## Usage

```
npm install --save caligram-react
```

To customize icons or text strings, pass a configuration object to caligram-react using the `config` method.

The `icons` configuration object expects React components. You can use a webpack loader to convert SVGs to React components, like [`SVGR`](https://github.com/smooth-code/svgr#webpack-usage).

The `linkComponent` expects a React component with `to` and `onClick` props. The `to` prop expects an object with `query` and `pathname` keys. This matches the `react-router` 2.0 Link component and might change in the future.

```javascript
import { CaligramReact } from 'caligram-react';
import { Link } from 'react-router';
import customIcon from './images/icons/customIcon.svg';
import iconOverride from './images/icons/iconOverride.svg';

CaligramReact.config({
  icons: {
    customIcon: customIcon,
    search: iconOverride, // You can also override the default icons supplied by caligram-react
  },
  linkComponent: Link,
  locales: {
    {
      fr: {
        price: 'Accès',
      },
    },
  },
});
```

## Contributing

After you clone the repository, you can begin developement and see your changes immediately using Storybook:

```
npm run storybook
```

If you want to use your developement copy of `caligram-react` in your project, we recommend using `wml` as Webpack doesn’t play nice with `npm link`. To use `wml` you’ll first need to [install Watchman](https://facebook.github.io/watchman/docs/install.html). Then install `wml` globally:

```
npm install -g wml
```

Then add the link to your project’s `node_modules` (you only need to do this once):

```
wml add ~/path/to/caligram-react ~/path/to/your-project/node_modules/caligram-react
```

Then run the `prepare` script to transpile to ES5, and start `wml` to sync the `dist` directory:

```
npm run prepare
wml start
```

`wml` will ask you if you want to ignore the `.git` and `node_modules` directories. Answer Yes for both.

Note that `wml start` will watch your `dist` directory and automatically sync changes; you can leave it running and run `npm run prepare` whenever you’ve made changes.

### Testing

Run tests with the following commands:

- `npm run testonly` – run tests once
- `npm run test-watch` – run tests and watch for changes
- `npm test` – run tests and apply lint rules

### Updating the npm package

```
npm run prepare
npm version (major|minor|patch)
npm publish
git push origin --tags
```

This changes the version number in `package.json`, creates a new tag, and pushes to npm.

See [full documentation](https://docs.npmjs.com/getting-started/publishing-npm-packages) on npm.

### Thanks

`caligram-react` is based on [React Component Development Kit](https://github.com/kadirahq/react-cdk).
