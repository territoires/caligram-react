const path = require('path');

module.exports = {
  module: {
    rules: [
      {
        test: /.(scss|css)$/,
        loader: 'style-loader!css-loader!sass-loader',
        include: path.resolve(__dirname, '../')
      }, {
        test: /\.png$/,
        loader: "url-loader"
      }, {
        test: /\.svg$/,
        loader: "babel-loader!@svgr/webpack"
      }
    ]
  },
  externals: {
    'jsdom': 'window',
    'react/lib/ExecutionEnvironment': true,
    'react/lib/ReactContext': 'window',
    'react/addons': true,
  }
}
