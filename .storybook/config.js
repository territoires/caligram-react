import React from 'react';
import { configure, addDecorator } from '@storybook/react';
import { withInfo, setDefaults } from '@storybook/addon-info';
import Enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import { storiesOf, action, linkTo, specs, describe, it } from "./facade";
import WebFont from 'webfontloader';
import '../src/initialize';
import './styles.scss';

Enzyme.configure({ adapter: new Adapter() });

global.storiesOf = storiesOf;
global.action = action;
global.linkTo = linkTo;
global.specs = specs;
global.describe = describe;
global.it = it;

WebFont.load({
  google: {
    families: ['Source Sans Pro:300,300italic,600,600italic'],
  },
});

const req = require.context('../src/stories', true, /.js$/);
function loadStories() {
  req.keys().forEach(filename => req(filename));
}

setDefaults({
  header: false,
  inline: true,
});

addDecorator((story, context) => withInfo()(story)(context));
configure(loadStories, module);
