import requireHacker from 'require-hacker';
import Enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import {storiesOf, action, linkTo, specs, describe, it, requestAnimationFrame} from "./mock";
import '../src/initialize';

Enzyme.configure({ adapter: new Adapter() });

global.storiesOf = storiesOf;
global.action = action;
global.linkTo = linkTo;
global.specs = specs;
global.describe = describe;
global.it = it;
global.requestAnimationFrame = requestAnimationFrame;

const reactSVGComponent = `
  require('create-react-class')({
    render() {
      return require('react').createElement('svg');
    }
  })
`;
requireHacker.hook('svg', () => `module.exports = ${reactSVGComponent}`);
