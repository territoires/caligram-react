process.env.NODE_ENV = 'development';
require('babel-core/register');
require('babel-polyfill');
require('raf/polyfill');
var JSDOM = require('jsdom').JSDOM;

global.document = (new JSDOM('<!doctype html><html><body></body></html>', {
 headers: {
   'User-Agent':
   'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_6_7)' +
   ' AppleWebKit/534.24 (KHTML, like Gecko) Chrome/11.0.696.71 Safari/534.24'
 }
})).window.document;
global.window = document.defaultView;
global.navigator = global.window.navigator;
global.addEventListener = global.window.addEventListener;

var exposedProperties = ['window', 'navigator', 'document'];

process.on('unhandledRejection', function (error) {
  console.error('Unhandled Promise Rejection:');
  console.error(error && error.stack || error);
});

require('./pretest.js');
